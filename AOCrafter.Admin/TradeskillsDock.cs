﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

using AOCrafter.Core;
using AOCrafter.Core.Controls;
using Twp.Data.AO;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class TradeskillsDock : DockContent
    {
        public TradeskillsDock()
        {
            InitializeComponent();
        }

        public void ShowAll()
        {
            this.OnSearch( this, new SearchEventArgs( new string[] { "ALL" } ) );
        }

        private void OnSearch( object sender, SearchEventArgs e )
        {
            this.resultList.Items.Clear();
            this.progressIndicator.Show();
            this.progressIndicator.Start();
            using( BackgroundWorker bw = new BackgroundWorker() )
            {
                bw.DoWork += new DoWorkEventHandler( DoSearch );
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler( OnSearchCompleted );
                bw.RunWorkerAsync( e.Words );
            }
        }

        private void DoSearch( object sender, DoWorkEventArgs e )
        {
            string[] args = e.Argument as string[];
            if( args == null || args.Length == 0 )
                return;
            else if( args[0].ToUpperInvariant() == "ALL" )
                e.Result = Tradeskill.GetAll();
            else
                e.Result = Tradeskill.Search( args );
        }

        private void OnSearchCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            TradeskillList tradeskills = e.Result as TradeskillList;

            if( tradeskills == null || tradeskills.Count == 0 )
            {
                this.countLabel.Text = "No tradeskills found.";
            }
            else
            {
                this.resultList.BeginUpdate();
                foreach( Tradeskill tradeskill in tradeskills )
                {
                    this.listView_Add( tradeskill );
                    Application.DoEvents();
                }
                this.countLabel.Text = "Tradeskills found: " + tradeskills.Count.ToString();
                this.resultList.EndUpdate();
            }
            this.progressIndicator.Stop();
            this.progressIndicator.Hide();
        }

        private void listView_Add( Tradeskill tradeskill )
        {
            //Log.Debug( "[TradeskillListBox.listView_Add] Id: {0}", tradeskill.Id );
            ListViewItem listItem = new ListViewItem();
            listItem.SubItems.Add( tradeskill.Id.ToString() );
            listItem.SubItems.Add( tradeskill.GroupName );
            listItem.SubItems.Add( tradeskill.Name );
            listItem.Tag = tradeskill;

            if( tradeskill.Processes.Count > 0 )
            {
                Item item = tradeskill.Processes[0].Result.GetNearest( 100 );
                if( item == null )
                    return;
                AOItem aoitem = item.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
                this.icons_Add( aoitem.Icon );
                listItem.ImageKey = aoitem.Icon.ToString();
            }

            this.resultList.Items.Add( listItem );
        }

        private void icons_Add( int id )
        {
            //Log.Debug( "[TradeskillListBox.icons_Add] Id: {0}", id );
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
        }

#region Drag & Drop

        private bool mouseButtonDown = false;

        private void resultList_MouseDown( object sender, MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
                this.mouseButtonDown = true;
        }

        private void resultList_MouseMove( object sender, MouseEventArgs e )
        {
            if( this.mouseButtonDown == false )
                return;

            if( e.Button == MouseButtons.Left )
            {
                TradeskillList tradeskills = new TradeskillList();
                foreach( ListViewItem listItem in this.resultList.SelectedItems )
                {
                    tradeskills.Add( (Tradeskill) listItem.Tag );
                }
                this.resultList.DoDragDrop( tradeskills, DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

#endregion

        [Category( "Behavior" )]
        [Description( "Occurs when one or more items are activated in the list, after a Tradeskill search" )]
        public event ItemsEventHandler OpenTradeskills;

        protected virtual void OnOpenTradeskills( ItemsEventArgs e )
        {
            if( this.OpenTradeskills != null )
                this.OpenTradeskills( this, e );
        }

        private void resultList_ItemActivate( object sender, EventArgs e )
        {
            this.OpenTradeskill();
        }

        private void openToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.OpenTradeskill();
        }

        private void OpenTradeskill()
        {
            TradeskillList tradeskills = new TradeskillList();
            foreach( ListViewItem listItem in this.resultList.SelectedItems )
            {
                tradeskills.Add( (Tradeskill) listItem.Tag );
            }
            this.OnOpenTradeskills( new ItemsEventArgs( tradeskills.ToArray() ) );
        }
    }
}
