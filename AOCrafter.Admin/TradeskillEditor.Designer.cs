﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class TradeskillEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

#region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( TradeskillEditor ) );
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processTree = new System.Windows.Forms.TreeView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findSimilarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshNodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.icons = new System.Windows.Forms.ImageList( this.components );
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.groupLabel = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.ComboBox();
            this.expansionBox = new System.Windows.Forms.ComboBox();
            this.expansionLabel = new System.Windows.Forms.Label();
            this.propertiesBox = new System.Windows.Forms.GroupBox();
            this.noRecursionBox = new System.Windows.Forms.CheckBox();
            this.previewBox = new System.Windows.Forms.GroupBox();
            this.autoUpdatePreviewBox = new System.Windows.Forms.CheckBox();
            this.updatePreviewButton = new System.Windows.Forms.Button();
            this.previewQlLabel = new System.Windows.Forms.Label();
            this.previewQlSlider = new Twp.Controls.Slider();
            this.previewProcessBox = new AOCrafter.Core.Controls.ProcessBox();
            this.toolTip = new System.Windows.Forms.ToolTip( this.components );
            this.contextMenuStrip.SuspendLayout();
            this.propertiesBox.SuspendLayout();
            this.previewBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // processTree
            // 
            this.processTree.AllowDrop = true;
            this.processTree.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
            | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.processTree.CheckBoxes = true;
            this.processTree.ContextMenuStrip = this.contextMenuStrip;
            this.processTree.FullRowSelect = true;
            this.processTree.HideSelection = false;
            this.processTree.ImageIndex = 0;
            this.processTree.ImageList = this.icons;
            this.processTree.Indent = 20;
            this.processTree.Location = new System.Drawing.Point( 6, 82 );
            this.processTree.Name = "processTree";
            this.processTree.SelectedImageIndex = 0;
            this.processTree.ShowPlusMinus = false;
            this.processTree.ShowRootLines = false;
            this.processTree.Size = new System.Drawing.Size( 389, 342 );
            this.processTree.TabIndex = 3;
            this.processTree.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler( this.ProcessTreeBeforeCheck );
            this.processTree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler( this.OnProcessChecked );
            this.processTree.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler( this.ProcessTreeBeforeCollapse );
            this.processTree.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler( this.ProcessTreeBeforeExpand );
            this.processTree.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler( this.ProcessTreeBeforeSelect );
            this.processTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler( this.ProcessTreeAfterSelect );
            this.processTree.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler( this.OnProcessDoubleClicked );
            this.processTree.DragDrop += new System.Windows.Forms.DragEventHandler( this.OnDragDrop );
            this.processTree.DragEnter += new System.Windows.Forms.DragEventHandler( this.OnDragEnter );
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
                this.openToolStripMenuItem,
                this.findSimilarToolStripMenuItem,
                this.refreshNodesToolStripMenuItem,
                this.toolStripSeparator,
                this.deleteToolStripMenuItem
            } );
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size( 149, 70 );
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler( this.OnOpen );
            // 
            // findSimilarToolStripMenuItem
            // 
            this.findSimilarToolStripMenuItem.Name = "findSimilarToolStripMenuItem";
            this.findSimilarToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.findSimilarToolStripMenuItem.Text = "&Find similar";
            this.findSimilarToolStripMenuItem.Click += new System.EventHandler( this.OnFindSimilar );
            // 
            // refreshNodesToolStripMenuItem
            // 
            this.refreshNodesToolStripMenuItem.Name = "refreshNodesToolStripMenuItem";
            this.refreshNodesToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.refreshNodesToolStripMenuItem.Text = "&Refresh nodes";
            this.refreshNodesToolStripMenuItem.Click += new System.EventHandler( this.OnRefreshNodesClicked );
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size( 190, 6 );
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size( 193, 22 );
            this.deleteToolStripMenuItem.Text = "&Delete process(es)";
            this.deleteToolStripMenuItem.Click += new System.EventHandler( this.OnDeleteProcessClicked );
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.icons.ImageSize = new System.Drawing.Size( 16, 16 );
            this.icons.TransparentColor = System.Drawing.Color.Lime;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point( 6, 18 );
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size( 38, 13 );
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "&Name:";
            // 
            // nameBox
            // 
            this.nameBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.nameBox.Location = new System.Drawing.Point( 48, 15 );
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size( 143, 20 );
            this.nameBox.TabIndex = 1;
            this.nameBox.TextChanged += new System.EventHandler( this.OnNameChanged );
            // 
            // groupLabel
            // 
            this.groupLabel.AutoSize = true;
            this.groupLabel.Location = new System.Drawing.Point( 6, 44 );
            this.groupLabel.Name = "groupLabel";
            this.groupLabel.Size = new System.Drawing.Size( 39, 13 );
            this.groupLabel.TabIndex = 2;
            this.groupLabel.Text = "&Group:";
            // 
            // groupBox
            // 
            this.groupBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.groupBox.FormattingEnabled = true;
            this.groupBox.Location = new System.Drawing.Point( 48, 41 );
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size( 143, 21 );
            this.groupBox.TabIndex = 3;
            this.groupBox.TextChanged += new System.EventHandler( this.OnGroupChanged );
            // 
            // expansionBox
            // 
            this.expansionBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.expansionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.expansionBox.FormattingEnabled = true;
            this.expansionBox.Location = new System.Drawing.Point( 262, 41 );
            this.expansionBox.Name = "expansionBox";
            this.expansionBox.Size = new System.Drawing.Size( 121, 21 );
            this.expansionBox.TabIndex = 5;
            this.expansionBox.SelectedIndexChanged += new System.EventHandler( this.OnExpansionChanged );
            this.expansionBox.Format += new System.Windows.Forms.ListControlConvertEventHandler( this.OnFormatExpansion );
            // 
            // expansionLabel
            // 
            this.expansionLabel.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.expansionLabel.AutoSize = true;
            this.expansionLabel.Location = new System.Drawing.Point( 197, 44 );
            this.expansionLabel.Name = "expansionLabel";
            this.expansionLabel.Size = new System.Drawing.Size( 59, 13 );
            this.expansionLabel.TabIndex = 4;
            this.expansionLabel.Text = "&Expansion:";
            // 
            // propertiesBox
            // 
            this.propertiesBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.propertiesBox.Controls.Add( this.noRecursionBox );
            this.propertiesBox.Controls.Add( this.nameLabel );
            this.propertiesBox.Controls.Add( this.nameBox );
            this.propertiesBox.Controls.Add( this.groupLabel );
            this.propertiesBox.Controls.Add( this.groupBox );
            this.propertiesBox.Controls.Add( this.expansionLabel );
            this.propertiesBox.Controls.Add( this.expansionBox );
            this.propertiesBox.Location = new System.Drawing.Point( 6, 6 );
            this.propertiesBox.Name = "propertiesBox";
            this.propertiesBox.Size = new System.Drawing.Size( 389, 70 );
            this.propertiesBox.TabIndex = 2;
            this.propertiesBox.TabStop = false;
            this.propertiesBox.Text = "Properties";
            // 
            // noRecursionBox
            // 
            this.noRecursionBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.noRecursionBox.AutoSize = true;
            this.noRecursionBox.Location = new System.Drawing.Point( 270, 17 );
            this.noRecursionBox.Name = "noRecursionBox";
            this.noRecursionBox.Size = new System.Drawing.Size( 109, 17 );
            this.noRecursionBox.TabIndex = 6;
            this.noRecursionBox.Text = "Prevent recursion";
            this.toolTip.SetToolTip( this.noRecursionBox, resources.GetString( "noRecursionBox.ToolTip" ) );
            this.noRecursionBox.UseVisualStyleBackColor = true;
            this.noRecursionBox.CheckedChanged += new System.EventHandler( this.OnLinkProcessesChanged );
            // 
            // previewBox
            // 
            this.previewBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.previewBox.Controls.Add( this.autoUpdatePreviewBox );
            this.previewBox.Controls.Add( this.updatePreviewButton );
            this.previewBox.Controls.Add( this.previewQlLabel );
            this.previewBox.Controls.Add( this.previewQlSlider );
            this.previewBox.Controls.Add( this.previewProcessBox );
            this.previewBox.Location = new System.Drawing.Point( 401, 6 );
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size( 400, 417 );
            this.previewBox.TabIndex = 4;
            this.previewBox.TabStop = false;
            this.previewBox.Text = "Preview";
            // 
            // autoUpdatePreviewBox
            // 
            this.autoUpdatePreviewBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.autoUpdatePreviewBox.AutoSize = true;
            this.autoUpdatePreviewBox.Checked = true;
            this.autoUpdatePreviewBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoUpdatePreviewBox.Location = new System.Drawing.Point( 6, 392 );
            this.autoUpdatePreviewBox.Name = "autoUpdatePreviewBox";
            this.autoUpdatePreviewBox.Size = new System.Drawing.Size( 86, 17 );
            this.autoUpdatePreviewBox.TabIndex = 3;
            this.autoUpdatePreviewBox.Text = "&Auto Update";
            this.autoUpdatePreviewBox.UseVisualStyleBackColor = true;
            // 
            // updatePreviewButton
            // 
            this.updatePreviewButton.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.updatePreviewButton.Location = new System.Drawing.Point( 319, 388 );
            this.updatePreviewButton.Name = "updatePreviewButton";
            this.updatePreviewButton.Size = new System.Drawing.Size( 75, 23 );
            this.updatePreviewButton.TabIndex = 4;
            this.updatePreviewButton.Text = "&Update";
            this.updatePreviewButton.UseVisualStyleBackColor = true;
            this.updatePreviewButton.Click += new System.EventHandler( this.OnUpdatePreviewClicked );
            // 
            // previewQlLabel
            // 
            this.previewQlLabel.AutoSize = true;
            this.previewQlLabel.Location = new System.Drawing.Point( 6, 23 );
            this.previewQlLabel.Name = "previewQlLabel";
            this.previewQlLabel.Size = new System.Drawing.Size( 24, 13 );
            this.previewQlLabel.TabIndex = 0;
            this.previewQlLabel.Text = "QL:";
            // 
            // previewQlSlider
            // 
            this.previewQlSlider.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.previewQlSlider.LabelPosition = Twp.Controls.LabelPosition.LabelBeforeSlider;
            this.previewQlSlider.Location = new System.Drawing.Point( 52, 19 );
            this.previewQlSlider.Name = "previewQlSlider";
            this.previewQlSlider.Size = new System.Drawing.Size( 342, 21 );
            this.previewQlSlider.TabIndex = 1;
            this.previewQlSlider.ValueChanged += new System.EventHandler( this.PreviewQlSliderValueChanged );
            // 
            // previewProcessBox
            // 
            this.previewProcessBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
            | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.previewProcessBox.Location = new System.Drawing.Point( 6, 46 );
            this.previewProcessBox.Name = "previewProcessBox";
            this.previewProcessBox.Size = new System.Drawing.Size( 388, 336 );
            this.previewProcessBox.TabIndex = 2;
            // 
            // TradeskillEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 807, 430 );
            this.ControlBox = false;
            this.Controls.Add( this.previewBox );
            this.Controls.Add( this.propertiesBox );
            this.Controls.Add( this.processTree );
            this.Name = "TradeskillEditor";
            this.Text = "Tradeskill Editor";
            this.Controls.SetChildIndex( this.processTree, 0 );
            this.Controls.SetChildIndex( this.propertiesBox, 0 );
            this.Controls.SetChildIndex( this.previewBox, 0 );
            this.contextMenuStrip.ResumeLayout( false );
            this.propertiesBox.ResumeLayout( false );
            this.propertiesBox.PerformLayout();
            this.previewBox.ResumeLayout( false );
            this.previewBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        private System.Windows.Forms.ToolStripMenuItem refreshNodesToolStripMenuItem;
        private System.Windows.Forms.Button updatePreviewButton;
        private System.Windows.Forms.CheckBox autoUpdatePreviewBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private Twp.Controls.Slider previewQlSlider;
        private System.Windows.Forms.Label previewQlLabel;
        private AOCrafter.Core.Controls.ProcessBox previewProcessBox;
        private System.Windows.Forms.GroupBox previewBox;
        private System.Windows.Forms.TreeView processTree;
        private System.Windows.Forms.ImageList icons;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findSimilarToolStripMenuItem;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label groupLabel;
        private System.Windows.Forms.ComboBox groupBox;
        private System.Windows.Forms.ComboBox expansionBox;
        private System.Windows.Forms.Label expansionLabel;
        private System.Windows.Forms.GroupBox propertiesBox;
        private System.Windows.Forms.CheckBox noRecursionBox;
        private System.Windows.Forms.ToolTip toolTip;
#endregion
    }
}