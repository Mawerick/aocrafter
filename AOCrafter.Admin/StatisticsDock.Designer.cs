﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class StatisticsDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.autoRefreshButton = new System.Windows.Forms.CheckBox();
            this.statsBox = new Twp.Controls.FillListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.intervalBox = new System.Windows.Forms.NumericUpDown();
            this.intervalLabel = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.updateButton = new System.Windows.Forms.Button();
            this.progressIndicator = new Twp.Controls.ProgressIndicator();
            ((System.ComponentModel.ISupportInitialize)(this.intervalBox)).BeginInit();
            this.SuspendLayout();
            // 
            // autoRefreshButton
            // 
            this.autoRefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.autoRefreshButton.AutoSize = true;
            this.autoRefreshButton.Checked = true;
            this.autoRefreshButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoRefreshButton.Location = new System.Drawing.Point(7, 361);
            this.autoRefreshButton.Name = "autoRefreshButton";
            this.autoRefreshButton.Size = new System.Drawing.Size(83, 17);
            this.autoRefreshButton.TabIndex = 1;
            this.autoRefreshButton.Text = "Auto-refresh";
            this.autoRefreshButton.UseVisualStyleBackColor = true;
            this.autoRefreshButton.CheckedChanged += new System.EventHandler(this.OnAutoRefreshToggled);
            // 
            // statsBox
            // 
            this.statsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
                                    | System.Windows.Forms.AnchorStyles.Left) 
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.statsBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                    this.columnHeader1,
                                    this.columnHeader2});
            this.statsBox.Location = new System.Drawing.Point(0, 0);
            this.statsBox.Name = "statsBox";
            this.statsBox.Size = new System.Drawing.Size(321, 356);
            this.statsBox.TabIndex = 0;
            this.statsBox.UseCompatibleStateImageBehavior = false;
            this.statsBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Data type";
            this.columnHeader1.Width = 257;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Count";
            // 
            // intervalBox
            // 
            this.intervalBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.intervalBox.Location = new System.Drawing.Point(266, 360);
            this.intervalBox.Maximum = new decimal(new int[] {
                                    120,
                                    0,
                                    0,
                                    0});
            this.intervalBox.Minimum = new decimal(new int[] {
                                    1,
                                    0,
                                    0,
                                    0});
            this.intervalBox.Name = "intervalBox";
            this.intervalBox.Size = new System.Drawing.Size(50, 20);
            this.intervalBox.TabIndex = 5;
            this.intervalBox.Value = new decimal(new int[] {
                                    30,
                                    0,
                                    0,
                                    0});
            this.intervalBox.Visible = false;
            this.intervalBox.ValueChanged += new System.EventHandler(this.OnIntervalChanged);
            // 
            // intervalLabel
            // 
            this.intervalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.intervalLabel.AutoSize = true;
            this.intervalLabel.Location = new System.Drawing.Point(208, 362);
            this.intervalLabel.Name = "intervalLabel";
            this.intervalLabel.Size = new System.Drawing.Size(45, 13);
            this.intervalLabel.TabIndex = 3;
            this.intervalLabel.Text = "Interval:";
            this.intervalLabel.Visible = false;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.OnTick);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.DoUpdate);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnUpdateCompleted);
            // 
            // updateButton
            // 
            this.updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateButton.Location = new System.Drawing.Point(241, 360);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 20);
            this.updateButton.TabIndex = 4;
            this.updateButton.Text = "Update";
            this.updateButton.UseCompatibleTextRendering = true;
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.OnUpdateButtonClicked);
            // 
            // progressIndicator
            // 
            this.progressIndicator.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressIndicator.CircleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressIndicator.Circles = ((uint)(8u));
            this.progressIndicator.Location = new System.Drawing.Point(148, 357);
            this.progressIndicator.Name = "progressIndicator";
            this.progressIndicator.Size = new System.Drawing.Size(24, 24);
            this.progressIndicator.TabIndex = 2;
            this.progressIndicator.Visible = false;
            this.progressIndicator.VisibleCircles = ((uint)(7u));
            // 
            // StatisticsDock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 384);
            this.Controls.Add(this.progressIndicator);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.intervalLabel);
            this.Controls.Add(this.intervalBox);
            this.Controls.Add(this.statsBox);
            this.Controls.Add(this.autoRefreshButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Location = new System.Drawing.Point(5, 0);
            this.Name = "StatisticsDock";
            this.Text = "Statistics";
            ((System.ComponentModel.ISupportInitialize)(this.intervalBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        private Twp.Controls.ProgressIndicator progressIndicator;
        private System.Windows.Forms.Button updateButton;

        #endregion

        private System.Windows.Forms.CheckBox autoRefreshButton;
        private Twp.Controls.FillListView statsBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.NumericUpDown intervalBox;
        private System.Windows.Forms.Label intervalLabel;
        private System.Windows.Forms.Timer timer;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}
