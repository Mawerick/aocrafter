﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Data.AO;
using Twp.Utilities;
using Twp.Utilities.Undo;

namespace AOCrafter.Admin
{
    public partial class TradeskillEditor : DataEditor
    {
#region Constructor

        public TradeskillEditor()
        {
            InitializeComponent();
            this.expansionBox.DataSource = Enum.GetValues( typeof( Expansions ) );
            this.editToolStripMenuItem.DropDownItems.AddRange( new ToolStripItem[] {
                this.toolStripSeparator,
                this.deleteToolStripMenuItem,
            } );
        }

#endregion

#region Properties

        private Tradeskill Tradeskill
        {
            get { return (Tradeskill) this.Data; }
        }

        public override string HelpText
        {
            get { return Properties.Resources.TradeskillHelp; }
        }

        public override string DefaultSearchType
        {
            get { return "Process"; }
        }

#endregion

#region Data update

        private void OnFormatExpansion( object sender, ListControlConvertEventArgs e )
        {
            e.Value = Tradeskill.GetExpansionName( (Expansions) e.Value );
        }

        protected override void OnDataChanged()
        {
            base.OnDataChanged();

            this.Tradeskill.PropertyChanged += this.OnTradeskillChanged;

            this.BeginUpdate();

            this.nameBox.Text = this.Tradeskill.Name;
            this.groupBox.DataSource = Tradeskill.GroupNames;
            if( String.IsNullOrEmpty( this.Tradeskill.GroupName ) && Tradeskill.GroupNames.Count > 0 )
                this.Tradeskill.GroupName = Tradeskill.GroupNames[0];
            this.groupBox.Text = this.Tradeskill.GroupName;
            this.expansionBox.SelectedItem = this.Tradeskill.Expansion;
            this.noRecursionBox.Checked = Flag.IsSet( this.Tradeskill.Flags, TradeskillFlags.NoRecursion );

            this.UpdateProcessTree();

            if( this.Tradeskill.Processes.Count > 8 )
                this.autoUpdatePreviewBox.Checked = false;
            if( this.autoUpdatePreviewBox.Checked )
                this.UpdatePreview();

            this.EndUpdate();
        }

        private void OnDeleteProcessClicked( object sender, EventArgs e )
        {
            if( this.Tradeskill == null )
                return;

            TreeNode node = this.processTree.SelectedNode;
            if( node == null )
                return;

            ProcessNode processNode = (ProcessNode) node.Tag;
            if( processNode == null )
                return;

            ProcessList list = new ProcessList();
            list.Add( processNode.Process );
            this.ActionManager.RecordAction( new AddItemAction<ProcessList>( this.RemoveProcessesAction, this.AddProcessesAction, list ) );
        }

        private void OnTradeskillChanged( object sender, PropertyChangedEventArgs e )
        {
            this.BeginUpdate();
            switch( e.PropertyName )
            {
                case "Name":
                    this.nameBox.Text = this.Tradeskill.Name;
                    break;
                case "GroupName":
                    this.groupBox.Text = this.Tradeskill.GroupName;
                    break;
                case "Expansion":
                    this.expansionBox.SelectedItem = this.Tradeskill.Expansion;
                    break;
                case "Flags":
                    this.noRecursionBox.Checked = Flag.IsSet( this.Tradeskill.Flags, TradeskillFlags.NoRecursion );
                    break;
            }
            this.EndUpdate();
        }

        private void OnNameChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.ActionManager.RecordAction( new SetPropertyAction( this.Tradeskill, "Name", this.nameBox.Text ) );
        }

        private void OnGroupChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.ActionManager.RecordAction( new SetPropertyAction( this.Tradeskill, "GroupName", this.groupBox.Text ) );
        }

        private void OnExpansionChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.ActionManager.RecordAction( new SetPropertyAction( this.Tradeskill, "Expansion", this.expansionBox.SelectedItem ) );
        }

        private void OnLinkProcessesChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.RecordFlagsAction( TradeskillFlags.NoRecursion, this.noRecursionBox.Checked );
        }

        private void RecordFlagsAction( TradeskillFlags flag, bool set )
        {
            TradeskillFlags flags = this.Tradeskill.Flags;
            if( set )
                flags |= flag;
            else
                flags &= ~flag;

            this.ActionManager.RecordAction( new SetPropertyAction( this.Tradeskill, "Flags", flags ) );
        }

#endregion

#region Drag & Drop

        private void OnDragEnter( object sender, DragEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( "Formats: " + String.Join( ", ", e.Data.GetFormats() ), "[TradeskillForm.DragEnter]" );
            if( e.Data.GetDataPresent( typeof( Process ) ) || e.Data.GetDataPresent( typeof( ProcessList ) ) )
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void OnDragDrop( object sender, DragEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( "Formats: " + String.Join( ", ", e.Data.GetFormats() ), "[TradeskillForm.DragDrop]" );
            if( e.Data.GetDataPresent( typeof( ProcessList ) ) )
            {
                ProcessList results = (ProcessList) e.Data.GetData( typeof( ProcessList ) );
                if( results != null )
                    this.ActionManager.RecordAction( new AddItemAction<ProcessList>( this.AddProcessesAction, this.RemoveProcessesAction, results ) );
            }
        }

        private void AddProcessesAction( ProcessList processes )
        {
            Log.Debug( "[TradeskillEditor.AddProcessesAction] Processes: {0}", processes != null ? processes.Count : -1 );
            if( processes == null || processes.Count == 0 )
                return;

            if( String.IsNullOrEmpty( this.Tradeskill.Name ) )
                this.Tradeskill.Name = processes[0].Result.ToString();

            foreach( Process process in processes )
            {
                if( !this.Tradeskill.Processes.Contains( process ) )
                {
                    Log.Debug( "[TradeskillEditor.AddProcessesAction] Adding process: {0}", process );
                    this.Tradeskill.Processes.Add( process );
                }
            }
            ProcessNodeCollection nodes = ProcessNodeCollection.Create( processes, !Flag.IsSet( this.Tradeskill.Flags, TradeskillFlags.NoRecursion ) );
            this.processTree.BeginUpdate();
            foreach( ProcessNode node in nodes )
            {
                if( !this.Tradeskill.Nodes.Contains( node ) )
                    this.Tradeskill.Nodes.Add( node );
                this.AddProcessNodeToTree( node );
            }
            this.processTree.EndUpdate();
            if( this.autoUpdatePreviewBox.Checked )
                this.UpdatePreview();
            Log.Debug( "[TradeskillEditor.AddProcessesAction] Done." );
        }

        private void RemoveProcessesAction( ProcessList processes )
        {
            if( processes == null || processes.Count == 0 )
                return;

            this.processTree.BeginUpdate();
            foreach( Process process in processes )
            {
                if( this.Tradeskill.Processes.Contains( process ) )
                    this.Tradeskill.Processes.Remove( process );
                if( this.Tradeskill.Nodes.Contains( process ) )
                {
                    this.RemoveProcessNodeFromTree( this.Tradeskill.Nodes[process] );
                    this.Tradeskill.Nodes.Remove( process );
                }
            }
            this.processTree.EndUpdate();
            if( this.autoUpdatePreviewBox.Checked )
                this.UpdatePreview();
        }

#endregion

#region ContextMenu

        private void OnOpen( object sender, EventArgs e )
        {
            TreeNode node = this.processTree.SelectedNode;
            if( node == null )
                return;

            ProcessList processes = new ProcessList();
            processes.Add( ( (ProcessNode) node.Tag ).Process );
            this.OnOpenEditors( new ItemsEventArgs( processes.ToArray() ) );
        }

        private void OnFindSimilar( object sender, EventArgs e )
        {
            TreeNode node = this.processTree.SelectedNode;
            if( node == null )
                return;

            Process process = ( (ProcessNode) node.Tag ).Process;
            ProcessList processes = Process.Search( process.Result, false );
            if( processes.Contains( process ) )
                processes.Remove( process );
            if( processes.Count == 0 )
            {
                string text = String.Format( "No processes were found where {0} is a result. Try creating one.", process.Result );
                MessageBox.Show( text, "No processes found", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
            else
            {
                this.OnOpenEditors( new ItemsEventArgs( processes.ToArray() ) );
            }
        }

        private void OnRefreshNodesClicked( object sender, EventArgs e )
        {
            this.Tradeskill.ResetNodes();
            this.UpdateProcessTree();
        }

#endregion

#region ProcessTree

        private Dictionary<int, ProcessNode> uniqueNodes = new Dictionary<int, ProcessNode>();
        private static readonly Color disabledColor = Color.LightGray;

        private void UpdateProcessTree()
        {
            this.processTree.BeginUpdate();
            this.processTree.Nodes.Clear();
            this.uniqueNodes.Clear();
            if( this.Tradeskill.Processes.Count <= 0 )
                return;

            foreach( ProcessNode node in this.Tradeskill.Nodes )
            {
                this.AddProcessNodeToTree( node );
                Application.DoEvents();
            }
            this.processTree.EndUpdate();
        }

        private void AddProcessNodeToTree( ProcessNode node )
        {
            this.AddProcessNodeToTree( node, null, String.Empty );
        }

        private void AddProcessNodeToTree( ProcessNode node, TreeNode parent, string prefix )
        {
            if( node == null )
                throw new ArgumentNullException( "node" );

            bool unique = true;
            if( !uniqueNodes.ContainsKey( node.Process.Id ) )
                uniqueNodes.Add( node.Process.Id, node );
            else
                unique = false;

            Item item = node.Process.Result.GetNearest( 100 );
            Log.Debug( "[TradeskillEditor.AddProcessNodeToTree] Node: {0} Item: {1}", node, item );
            if( item == null )
                return;

            AOItem aoitem = item.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
            int iconIndex = this.GetIcon( aoitem.Icon );

            TreeNode treeNode = new TreeNode();
#if DEBUG
            treeNode.Text = String.Format( "{0}{1} [{2}]", prefix, aoitem.Name, node.Process.Id );
#else
            treeNode.Text = prefix + aoitem.Name;
#endif
            treeNode.Tag = node;
            Log.Debug( "[TradeskillEditor.AddProcessNodeToTree] Linked? {0}", Flag.IsSet( node.Process.Flags, ProcessFlags.LinkProcess ) );
            treeNode.Checked = !Flag.IsSet( node.Process.Flags, ProcessFlags.LinkProcess );
            treeNode.ImageIndex = iconIndex;
            treeNode.SelectedImageIndex = iconIndex;
            if( unique )
            {
                foreach( ProcessNode source in node.Sources )
                    this.AddProcessNodeToTree( source, treeNode, "[S] " );
                foreach( ProcessNode target in node.Targets )
                    this.AddProcessNodeToTree( target, treeNode, "[T] " );
            }
            else
            {
                treeNode.ForeColor = disabledColor;
            }

            if( parent != null )
            {
                parent.Nodes.Add( treeNode );
                if( !Flag.IsSet( node.Process.Flags, ProcessFlags.LinkProcess ) )
                    treeNode.Expand();
                else
                    treeNode.Collapse();
            }
            else
            {
                this.processTree.Nodes.Add( treeNode );
                treeNode.Expand();
            }

            node.Process.PropertyChanged += delegate {
                this.BeginUpdate();
                treeNode.Checked = !Flag.IsSet( node.Process.Flags, ProcessFlags.LinkProcess );
                this.EndUpdate();
            };
        }

        private void RemoveProcessNodeFromTree( ProcessNode node )
        {
            int index = -1;
            for( int i = 0; i < this.processTree.Nodes.Count; i++ )
            {
                if( (ProcessNode) this.processTree.Nodes[i].Tag == node )
                {
                    index = i;
                    if( this.processTree.Nodes[i].ForeColor != disabledColor )
                        this.uniqueNodes.Remove( node.Process.Id );
                    break;
                }
            }
            if( index >= 0 )
                this.processTree.Nodes.RemoveAt( index );
        }

        private int GetIcon( int id )
        {
            System.Diagnostics.Debug.WriteLine( "Icon: " + id.ToString(), "[TradeskillEditor.GetIcon]" );
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
            return this.icons.Images.IndexOfKey( id.ToString() );
        }

        private void ProcessTreeBeforeSelect( object sender, TreeViewCancelEventArgs e )
        {
            if( e.Node.ForeColor == disabledColor )
                e.Cancel = true;
        }

        private void ProcessTreeAfterSelect( object sender, TreeViewEventArgs e )
        {
            this.deleteToolStripMenuItem.Enabled = ( e.Node != null && e.Node.Tag != null );
        }

        private void ProcessTreeBeforeCheck( object sender, TreeViewCancelEventArgs e )
        {
            if( !this.IsUpdating && e.Node.ForeColor == disabledColor )
                e.Cancel = true;
        }

        private void ProcessTreeBeforeExpand( object sender, TreeViewCancelEventArgs e )
        {
            if( e.Node != null || !e.Node.Checked )
                return;

            // Ignore expand requests
            e.Cancel = true;
        }

        private void ProcessTreeBeforeCollapse( object sender, TreeViewCancelEventArgs e )
        {
            if( e.Node != null || e.Node.Checked )
                return;

            // Ignore collapse requests
            e.Cancel = true;
        }

        private void OnProcessDoubleClicked( object sender, TreeNodeMouseClickEventArgs e )
        {
            TreeNode node = e.Node;
            if( node == null )
                return;

            ProcessList processes = new ProcessList();
            processes.Add( ( (ProcessNode) node.Tag ).Process );
            this.OnOpenEditors( new ItemsEventArgs( processes.ToArray() ) );
        }

        private void OnProcessChecked( object sender, TreeViewEventArgs e )
        {
            if( e.Node == null )
                return;

            if( this.IsUpdating )
                return;

            ProcessNode node = (ProcessNode) e.Node.Tag;

            ProcessFlags flags = node.Process.Flags;
            if( !e.Node.Checked )
                flags |= ProcessFlags.LinkProcess;
            else
                flags &= ~ProcessFlags.LinkProcess;

            node.Modified = true;
            this.ActionManager.RecordAction( new SetPropertyAction( node.Process, "Flags", flags ) );

            this.processTree.BeginUpdate();
            if( Flag.IsSet( node.Process.Flags, ProcessFlags.LinkProcess ) && e.Node.Parent != null )
                e.Node.Collapse();
            else
                e.Node.Expand();
            this.processTree.EndUpdate();
            if( this.autoUpdatePreviewBox.Checked )
                this.UpdatePreview();
        }

        private TreeNode FindProcessInTree( ProcessNode node, TreeNodeCollection treeNodes )
        {
            foreach( TreeNode treeNode in treeNodes )
            {
                if( ( treeNode.Tag as ProcessNode ) == node )
                    return treeNode;

                if( treeNode.Nodes.Count > 0 )
                {
                    TreeNode subNode = FindProcessInTree( node, treeNode.Nodes );
                    if( subNode != null )
                        return subNode;
                }
            }
            return null;
        }

#endregion

#region Preview

        private void OnUpdatePreviewClicked( object sender, EventArgs e )
        {
            this.UpdatePreview();
        }

        private void UpdatePreview()
        {
            Log.Debug( "[TradeskillEditor.UpdatePreview] Processes: {0}", this.Tradeskill.Processes.Count );

            if( this.Tradeskill.Processes.Count <= 0 )
                return;

            if( this.Tradeskill.LowQL > 0 )
            {
                this.previewQlSlider.Minimum = this.Tradeskill.LowQL;
                this.previewQlSlider.Maximum = this.Tradeskill.HighQL;
                this.previewQlSlider.Enabled = !this.Tradeskill.StaticQL;
            }
            else
            {
                this.previewQlSlider.Enabled = false;
            }

            this.previewProcessBox.Tradeskill = this.Tradeskill;
            this.QlChanged();
        }

        private void PreviewQlSliderValueChanged( object sender, EventArgs e )
        {
            this.QlChanged();
        }

        private void QlChanged()
        {
            this.previewProcessBox.Tradeskill.Calculate( (int) Decimal.Round( this.previewQlSlider.Value, MidpointRounding.ToEven ) );
            this.previewProcessBox.UpdateProcess();
        }

#endregion
    }
}
