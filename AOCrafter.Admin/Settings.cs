//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using Twp.Controls;
using Twp.Utilities;

namespace AOCrafter.Admin
{
    public class Settings
    {
        public Settings()
        {
            this.doc = new IniDocument( "AOCrafter.ini", Path.GetAppDataPath( "WrongPlace.Net", "AOCrafter" ) );
        }

        private IniDocument doc;

        public string AOPath
        {
            get { return this.doc["General"]["AOPath"]; }
            set { this.doc["General"]["AOPath"] = value; }
        }

        public string DataPath
        {
            get { return this.doc["General"]["DataPath"]; }
            set { this.doc["General"]["DataPath"] = value; }
        }

        public bool AddNewItemsToDropBox
        {
            get { return Convert.ToBoolean( this.doc["Admin"]["AddNewItemsToDropBox"] ); }
            set { this.doc["Admin"]["AddNewItemsToDropBox"] = value.ToString(); }
        }

        public bool StatsAutoRefresh
        {
            get { return Convert.ToBoolean( this.doc["Admin.Statistics"]["AutoRefresh"] ); }
            set { this.doc["Admin.Statistics"]["AutoRefresh"] = value.ToString(); }
        }

        public int StatsRefreshInterval
        {
            get { return Convert.ToInt32( this.doc["Admin.Statistics"]["RefreshInterval"] ); }
            set { this.doc["Admin.Statistics"]["RefreshInterval"] = value.ToString(); }
        }

        public void Load()
        {
            try
            {
                this.doc.Read();
            }
            catch( IniParseException ex )
            {
                Log.Warning( ex.Message );
                Log.Debug( "[Settings] [Load]", ex.Line );
            }
            this.doc.SetDefault( "General", "AOPath", String.Empty );
            this.doc.SetDefault( "General", "DataPath", Path.GetCommonAppDataPath() );
            this.doc.SetDefault( "Admin", "AddNewItemsToDropBox", "true" );
            this.doc.SetDefault( "Admin.Statistics", "AutoRefresh", "false" );
            this.doc.SetDefault( "Admin.Statistics", "RefreshInterval", "30" );
        }

        public void Save()
        {
            try
            {
                this.doc.Write();
            }
            catch( Exception ex )
            {
                ExceptionDialog.Show( ex );
            }
        }
    }
}
