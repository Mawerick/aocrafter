﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Data.AO;
using Twp.Utilities;
using Twp.Utilities.Undo;

namespace AOCrafter.Admin
{
    public partial class ItemEditor : DataEditor
    {
        #region Constructor

        public ItemEditor()
        {
            InitializeComponent();
            this.htmlPanel.BaseStylesheet = Properties.Resources.ItemCSS;
        }

        #endregion
        
        #region Private fields

        private ItemInfo itemInfo;

        #endregion

        #region Properties

        private ProcItem Item
        {
            get { return (ProcItem) this.Data; }
        }
        
        public override string HelpText
        {
            get { return Properties.Resources.ItemHelp; }
        }

        public override string DefaultSearchType
        {
            get { return "AOItem"; }
        }

        #endregion

        #region Data update

        protected override void OnDataChanged()
        {
            base.OnDataChanged();
            this.Item.Items.ListChanged += this.OnItemChanged;
            this.UpdateData();
        }

        private void UpdateData()
        {
            this.qlSlider.SetRange( this.Item.LowQL, this.Item.HighQL );
            this.qlSlider.Value = this.Item.HighQL;
            this.qlSlider.Enabled = (this.Item.LowQL != this.Item.HighQL);

            this.itemList.BeginUpdate();
            this.itemList.Items.Clear();
            foreach( AOItem item in this.Item.Items )
                this.AddItem( item );
            this.itemList.EndUpdate();

            this.UpdatePreview();
        }

        private void AddItem( AOItem item )
        {
            if( this.icons.Images.ContainsKey( item.Icon.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( item.Icon );
                if( icon != null )
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
            }

            ListViewItem listItem = new ListViewItem();
            listItem.SubItems.Add( item.Id.ToString() );
            listItem.SubItems.Add( item.Name );
            listItem.SubItems.Add( item.Ql.ToString() );
            listItem.ImageKey = item.Icon.ToString();
            listItem.Tag = item;

            this.itemList.Items.Add( listItem );
        }

        private void OnItemChanged( object sender, ListChangedEventArgs e )
        {
            this.qlSlider.SetRange( this.Item.LowQL, this.Item.HighQL );
            this.qlSlider.Enabled = (this.Item.LowQL != this.Item.HighQL);
            this.UpdatePreview();
        }

        private void QlSliderValueChanged( object sender, EventArgs e )
        {
            this.UpdatePreview();
        }

        private void UpdatePreview()
        {
            Item item = this.Item.GetNearest( Decimal.ToInt32( this.qlSlider.Value ) );
            if( item != null )
            {
                this.itemInfo = ItemParser.Interpolate( item );
                this.itemInfo.Database = Twp.Data.AO.DatabaseManager.ItemsDatabase;
                this.itemInfo.Mmdb = Core.DatabaseManager.Mmdb;
                this.htmlPanel.Text = "<html><body>" + this.itemInfo.ToHtml() + "</body></html>";
            }
            else
            {
                this.htmlPanel.Text = "<html><body></body></html>";
            }
        }

        private void HtmlPanelImageLoad( object sender, Twp.Controls.HtmlRenderer.Entities.HtmlImageLoadEventArgs e )
        {
            int index = e.Src.IndexOf( "://" );
            if( index >= 0 )
            {
                string scheme = e.Src.Substring( 0, index ).ToLower();
                string content = e.Src.Substring( index + 3 ).ToLower();
                if( scheme == "rdb" )
                {
                    try
                    {
                        AOIcon icon = AOIcon.Load( Convert.ToInt32( content ), this.itemInfo.Database );
                        System.Drawing.Image image = icon.Image;
                        if( image.Width == 64 && image.Height == 64 )
                            image = new Bitmap( image, 48, 48 );
                        e.Callback( image, new Rectangle( 0, 0, image.Width, image.Height ) );
                        e.Handled = true;
                    }
                    catch( Exception )
                    {
                    }
                }
                else if( scheme == "resource" )
                {
                    if( content == "itembg" )
                    {
                        System.Drawing.Image image = Properties.Resources.ItemBG;
                        e.Callback( image, new System.Drawing.Rectangle( 0, 0, image.Width, image.Height ) );
                        e.Handled = true;
                    }
                }
            }
        }

        private void HtmlPanelLinkClicked( object sender, Twp.Controls.HtmlRenderer.Entities.HtmlLinkClickedEventArgs e )
        {
            // TODO: Implement HtmlPanelLinkClicked
        }

        private void ResortItemsAction()
        {
            this.Item.Items.Sort();
            this.UpdateData();
        }

        #endregion

        #region Menu

        private void OnDeleteItemClicked( object sender, EventArgs e )
        {
            if( this.Item == null )
                return;

            ListView.SelectedListViewItemCollection items = this.itemList.SelectedItems;
            foreach( ListViewItem lvi in items )
            {
                AOItem item = (AOItem) lvi.Tag;
                this.ActionManager.RecordAction( new AddItemAction<AOItem>( this.RemoveItemAction, this.AddItemAction, item ) );
            }
        }

        private void OnResortItemsClicked( object sender, EventArgs e )
        {
            this.ActionManager.RecordAction( new CallMethodAction( this.ResortItemsAction, null ) );
        }

        #endregion

        #region Drag & Drop

        private void itemList_DragEnter( object sender, DragEventArgs e )
        {
            Log.Debug( "[ItemForm.DragEnter] Formats: " + String.Join( ", ", e.Data.GetFormats() ) );
            if( e.Data.GetDataPresent( typeof( AOItemList ) ) )
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void itemList_DragDrop( object sender, DragEventArgs e )
        {
            Log.Debug( "[ItemForm.DragDrop] Formats: " + String.Join( ", ", e.Data.GetFormats() ) );
            AOItemList items = (AOItemList) e.Data.GetData( typeof( AOItemList ) );
            Log.DebugIf( items == null, "[ItemForm.DragDrop] e.Data.GetData returns null!" );
            if( items != null )
            {
                this.itemList.BeginUpdate();
                items.Sort();
                Log.Debug( "[ItemForm.DragDrop] Items: " + items.Count.ToString() );
                foreach( AOItem item in items )
                {
                    Log.Debug( "[ItemForm.DragDrop] Adding: " + item.ToString() );
                    this.ActionManager.RecordAction( new AddItemAction<AOItem>( this.AddItemAction, this.RemoveItemAction, item ) );
                }
                this.itemList.EndUpdate();
            }
        }

        private void AddItemAction( AOItem item )
        {
            this.Item.Items.Add( item );
            this.Item.Items.Sort();
            this.UpdateData();
        }

        private void RemoveItemAction( AOItem item )
        {
            this.Item.Items.Remove( item );
            this.UpdateData();
        }

        #endregion
    }
}
