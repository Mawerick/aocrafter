﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class ItemEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.itemList = new System.Windows.Forms.ListView();
            this.iconHeader2 = new System.Windows.Forms.ColumnHeader();
            this.idHeader2 = new System.Windows.Forms.ColumnHeader();
            this.nameHeader2 = new System.Windows.Forms.ColumnHeader();
            this.qlHeader2 = new System.Windows.Forms.ColumnHeader();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.deleteItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.htmlPanel = new Twp.Controls.HtmlRenderer.HtmlPanel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.qlLabel = new System.Windows.Forms.Label();
            this.qlSlider = new Twp.Controls.Slider();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resortItemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // itemList
            // 
            this.itemList.AllowDrop = true;
            this.itemList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
                                    | System.Windows.Forms.AnchorStyles.Left) 
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.itemList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                    this.iconHeader2,
                                    this.idHeader2,
                                    this.nameHeader2,
                                    this.qlHeader2});
            this.itemList.ContextMenuStrip = this.contextMenuStrip;
            this.itemList.FullRowSelect = true;
            this.itemList.LargeImageList = this.icons;
            this.itemList.Location = new System.Drawing.Point(3, 3);
            this.itemList.Name = "itemList";
            this.itemList.ShowGroups = false;
            this.itemList.Size = new System.Drawing.Size(374, 377);
            this.itemList.SmallImageList = this.icons;
            this.itemList.TabIndex = 0;
            this.itemList.UseCompatibleStateImageBehavior = false;
            this.itemList.View = System.Windows.Forms.View.Details;
            this.itemList.DragDrop += new System.Windows.Forms.DragEventHandler(this.itemList_DragDrop);
            this.itemList.DragEnter += new System.Windows.Forms.DragEventHandler(this.itemList_DragEnter);
            // 
            // iconHeader2
            // 
            this.iconHeader2.Text = "";
            this.iconHeader2.Width = 20;
            // 
            // idHeader2
            // 
            this.idHeader2.Text = "ID";
            this.idHeader2.Width = 50;
            // 
            // nameHeader2
            // 
            this.nameHeader2.Text = "Name";
            this.nameHeader2.Width = 250;
            // 
            // qlHeader2
            // 
            this.qlHeader2.Text = "QL";
            this.qlHeader2.Width = 30;
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.icons.ImageSize = new System.Drawing.Size(16, 16);
            this.icons.TransparentColor = System.Drawing.Color.Lime;
            // 
            // deleteItemToolStripMenuItem
            // 
            this.deleteItemToolStripMenuItem.Name = "deleteItemToolStripMenuItem";
            this.deleteItemToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteItemToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteItemToolStripMenuItem.Text = "Delete item";
            this.deleteItemToolStripMenuItem.Click += new System.EventHandler(this.OnDeleteItemClicked);
            // 
            // htmlPanel
            // 
            this.htmlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
                                    | System.Windows.Forms.AnchorStyles.Left) 
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.htmlPanel.AutoScroll = true;
            this.htmlPanel.AvoidGeometryAntialias = false;
            this.htmlPanel.AvoidImagesLateLoading = false;
            this.htmlPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.htmlPanel.BaseStylesheet = null;
            this.htmlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.htmlPanel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.htmlPanel.Location = new System.Drawing.Point(3, 30);
            this.htmlPanel.Name = "htmlPanel";
            this.htmlPanel.Size = new System.Drawing.Size(400, 350);
            this.htmlPanel.TabIndex = 2;
            this.htmlPanel.LinkClicked += new System.EventHandler<Twp.Controls.HtmlRenderer.Entities.HtmlLinkClickedEventArgs>(this.HtmlPanelLinkClicked);
            this.htmlPanel.ImageLoad += new System.EventHandler<Twp.Controls.HtmlRenderer.Entities.HtmlImageLoadEventArgs>(this.HtmlPanelImageLoad);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
                                    | System.Windows.Forms.AnchorStyles.Left) 
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(6, 6);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.itemList);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.qlLabel);
            this.splitContainer.Panel2.Controls.Add(this.qlSlider);
            this.splitContainer.Panel2.Controls.Add(this.htmlPanel);
            this.splitContainer.Size = new System.Drawing.Size(790, 383);
            this.splitContainer.SplitterDistance = 380;
            this.splitContainer.TabIndex = 2;
            // 
            // qlLabel
            // 
            this.qlLabel.AutoSize = true;
            this.qlLabel.Location = new System.Drawing.Point(3, 7);
            this.qlLabel.Name = "qlLabel";
            this.qlLabel.Size = new System.Drawing.Size(24, 13);
            this.qlLabel.TabIndex = 0;
            this.qlLabel.Text = "QL:";
            // 
            // qlSlider
            // 
            this.qlSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.qlSlider.LabelPosition = Twp.Controls.LabelPosition.LabelBeforeSlider;
            this.qlSlider.Location = new System.Drawing.Point(33, 3);
            this.qlSlider.Maximum = new decimal(new int[] {
                                    100,
                                    0,
                                    0,
                                    0});
            this.qlSlider.Minimum = new decimal(new int[] {
                                    0,
                                    0,
                                    0,
                                    0});
            this.qlSlider.Name = "qlSlider";
            this.qlSlider.PageStep = new decimal(new int[] {
                                    10,
                                    0,
                                    0,
                                    0});
            this.qlSlider.Size = new System.Drawing.Size(370, 21);
            this.qlSlider.Step = new decimal(new int[] {
                                    1,
                                    0,
                                    0,
                                    0});
            this.qlSlider.TabIndex = 1;
            this.qlSlider.TickFrequency = new decimal(new int[] {
                                    5,
                                    0,
                                    0,
                                    0});
            this.qlSlider.Value = new decimal(new int[] {
                                    0,
                                    0,
                                    0,
                                    0});
            this.qlSlider.ValueChanged += new System.EventHandler(this.QlSliderValueChanged);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                    this.resortItemsToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(140, 26);
            // 
            // resortItemsToolStripMenuItem
            // 
            this.resortItemsToolStripMenuItem.Name = "resortItemsToolStripMenuItem";
            this.resortItemsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.resortItemsToolStripMenuItem.Text = "&Resort items";
            this.resortItemsToolStripMenuItem.Click += new System.EventHandler(this.OnResortItemsClicked);
            // 
            // ItemEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 395);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer);
            this.Name = "ItemEditor";
            this.Text = "Item Editor";
            this.Controls.SetChildIndex(this.splitContainer, 0);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        private System.Windows.Forms.ToolStripMenuItem resortItemsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private Twp.Controls.Slider qlSlider;
        private System.Windows.Forms.Label qlLabel;
        private System.Windows.Forms.SplitContainer splitContainer;
        private Twp.Controls.HtmlRenderer.HtmlPanel htmlPanel;

        #endregion

        private System.Windows.Forms.ListView itemList;
        private System.Windows.Forms.ColumnHeader idHeader2;
        private System.Windows.Forms.ColumnHeader nameHeader2;
        private System.Windows.Forms.ColumnHeader qlHeader2;
        private System.Windows.Forms.ToolStripMenuItem deleteItemToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader iconHeader2;
        private System.Windows.Forms.ImageList icons;
    }
}
