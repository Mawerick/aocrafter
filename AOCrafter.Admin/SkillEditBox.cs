﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Utilities.Undo;

namespace AOCrafter.Admin
{
    public partial class SkillEditBox : UserControl
    {
        public SkillEditBox( Skill skill, ActionManager actionManager )
        {
            InitializeComponent();

            this.actionManager = actionManager;
            this.keyList.DataSource = Enum.GetValues( typeof( SkillName ) );

            this.BeginUpdate();
            this.skill = skill;
            this.skill.PropertyChanged += this.OnPropertyChanged;
            this.keyList.SelectedItem = this.skill.Key;
            this.valueBox.Value = this.skill.Value;
            this.EndUpdate();
        }

        private Skill skill;
        private ActionManager actionManager;
        private bool updating = false;
        
        public Skill Skill
        {
            get { return this.skill; }
        }

        public event EventHandler Deleted;

        private void OnDeleteClicked( object sender, EventArgs e )
        {
            if( this.Deleted != null )
                this.Deleted( this, EventArgs.Empty );
        }

        private void OnFormatKey( object sender, ListControlConvertEventArgs e )
        {
            e.Value = Skill.GetName( (SkillName) e.Value );
        }

        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            this.BeginUpdate();
            switch( e.PropertyName )
            {
                case "Key":
                    this.keyList.SelectedItem = this.skill.Key;
                    break;
                case "Value":
                    this.valueBox.Value = this.skill.Value;
                    break;
            }
            this.EndUpdate();
        }

        private void OnKeyChanged( object sender, EventArgs e )
        {
            if( !this.updating )
                this.actionManager.RecordAction( new SetPropertyAction( this.skill, "Key", this.keyList.SelectedItem ) );
        }

        private void OnValueChanged( object sender, EventArgs e )
        {
            if( !this.updating )
                this.actionManager.RecordAction( new SetPropertyAction( this.skill, "Value", this.valueBox.Value ) );
        }
        
        private void BeginUpdate()
        {
            this.updating = true;
            this.SuspendLayout();
        }
        
        private void EndUpdate()
        {
            this.updating = false;
            this.ResumeLayout( false );
            this.PerformLayout();
        }
    }
}
