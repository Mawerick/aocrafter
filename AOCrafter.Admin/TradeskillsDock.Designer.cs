﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class TradeskillsDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.resultList = new Twp.Controls.FillListView();
            this.iconHeader = new System.Windows.Forms.ColumnHeader();
            this.idHeader = new System.Windows.Forms.ColumnHeader();
            this.groupHeader = new System.Windows.Forms.ColumnHeader();
            this.nameHeader = new System.Windows.Forms.ColumnHeader();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.countLabel = new System.Windows.Forms.Label();
            this.searchBox = new AOCrafter.Core.Controls.SearchBox();
            this.progressIndicator = new Twp.Controls.ProgressIndicator();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultList
            // 
            this.resultList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                    this.iconHeader,
                                    this.idHeader,
                                    this.groupHeader,
                                    this.nameHeader});
            this.resultList.ContextMenuStrip = this.contextMenuStrip;
            this.resultList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultList.FullRowSelect = true;
            this.resultList.LargeImageList = this.icons;
            this.resultList.Location = new System.Drawing.Point(0, 37);
            this.resultList.Name = "resultList";
            this.resultList.Size = new System.Drawing.Size(323, 283);
            this.resultList.SmallImageList = this.icons;
            this.resultList.StretchIndex = 3;
            this.resultList.TabIndex = 1;
            this.resultList.UseCompatibleStateImageBehavior = false;
            this.resultList.View = System.Windows.Forms.View.Details;
            this.resultList.ItemActivate += new System.EventHandler(this.resultList_ItemActivate);
            this.resultList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.resultList_MouseDown);
            this.resultList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.resultList_MouseMove);
            // 
            // iconHeader
            // 
            this.iconHeader.Text = "";
            this.iconHeader.Width = 26;
            // 
            // idHeader
            // 
            this.idHeader.Text = "ID";
            this.idHeader.Width = 50;
            // 
            // groupHeader
            // 
            this.groupHeader.Text = "Group";
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "Name";
            this.nameHeader.Width = 183;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                    this.openToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(104, 26);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.icons.ImageSize = new System.Drawing.Size(16, 16);
            this.icons.TransparentColor = System.Drawing.Color.Lime;
            // 
            // countLabel
            // 
            this.countLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.countLabel.Location = new System.Drawing.Point(0, 320);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(323, 16);
            this.countLabel.TabIndex = 3;
            this.countLabel.Text = "Search for something...";
            this.countLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // searchBox
            // 
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox.Location = new System.Drawing.Point(0, 0);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(323, 37);
            this.searchBox.TabIndex = 0;
            this.searchBox.Search += new AOCrafter.Core.Controls.SearchEventHandler(this.OnSearch);
            // 
            // progressIndicator
            // 
            this.progressIndicator.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressIndicator.BackColor = System.Drawing.SystemColors.Window;
            this.progressIndicator.CircleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressIndicator.Circles = ((uint)(12u));
            this.progressIndicator.CircleSize = 0.75F;
            this.progressIndicator.Location = new System.Drawing.Point(137, 154);
            this.progressIndicator.Name = "progressIndicator";
            this.progressIndicator.Size = new System.Drawing.Size(48, 48);
            this.progressIndicator.TabIndex = 2;
            this.progressIndicator.Visible = false;
            this.progressIndicator.VisibleCircles = ((uint)(11u));
            // 
            // TradeskillsDock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 336);
            this.Controls.Add(this.progressIndicator);
            this.Controls.Add(this.resultList);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.countLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "TradeskillsDock";
            this.Text = "Tradeskills";
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        private System.Windows.Forms.ColumnHeader groupHeader;
        private Twp.Controls.ProgressIndicator progressIndicator;

        #endregion

        private Twp.Controls.FillListView resultList;
        private System.Windows.Forms.ColumnHeader idHeader;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.ColumnHeader iconHeader;
        private System.Windows.Forms.ImageList icons;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.Label countLabel;
        private AOCrafter.Core.Controls.SearchBox searchBox;


    }
}
