﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using AOCrafter.Core;
using AOCrafter.Core.Controls;
using Twp.Data.AO;
using Twp.Utilities;
using Twp.Utilities.Undo;

namespace AOCrafter.Admin
{
    public partial class ProcessEditor : DataEditor
    {
        #region Constructor

        public ProcessEditor()
        {
            InitializeComponent();

            this.editToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
                                                                  this.toolStripSeparator1,
                                                                  this.sourceToolStripMenuItem,
                                                                  this.targetToolStripMenuItem,
                                                                  this.resultToolStripMenuItem,
                                                                  this.toolStripSeparator8,
                                                                  this.createCopyToolStripMenuItem
                                                              });
            this.DataSaved += this.OnProcessSaved;
        }

        #endregion

        #region Properties

        private Process Process
        {
            get { return (Process) this.Data; }
        }

        public override string HelpText
        {
            get { return Properties.Resources.ProcessHelp; }
        }

        public override string DefaultSearchType
        {
            get { return "Item"; }
        }

        #endregion

        #region Data update

        protected override void OnDataChanged()
        {
            base.OnDataChanged();

            this.Process.SourceChanged += this.OnSourceChanged;
            this.Process.TargetChanged += this.OnTargetChanged;
            this.Process.ResultChanged += this.OnResultChanged;
            this.Process.PropertyChanged += this.OnPropertyChanged;
            this.Process.Skills.ListChanged += this.OnSkillsChanged;

            this.BeginUpdate();
            this.UpdateSource();
            this.UpdateTarget();
            this.UpdateResult();

            this.consumeSourceToolStripMenuItem.Checked = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeSource );
            this.consumeTargetToolStripMenuItem.Checked = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeTarget );

            this.setSourceAsPrimaryObjectToolStripMenuItem.Enabled = !Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
            this.setTargetAsPrimaryObjectToolStripMenuItem.Enabled = Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );

            this.multiplierBox.Value = this.Process.Multiplier;
            this.notePanel.Text = this.Process.Note;

            this.skillPanel.SuspendLayout();
            this.skillPanel.Controls.Clear();
            foreach( Skill skill in this.Process.Skills )
                this.AddSkill( skill );
            this.skillPanel.ResumeLayout( false );
            this.skillPanel.PerformLayout();

            this.UpdateDateLabels();

            this.EndUpdate();
        }
        
        private void OnProcessSaved( object sender, EventArgs e )
        {
            this.UpdateDateLabels();
        }
        
        private void UpdateDateLabels()
        {
            DateTime date;
            if( DateTime.TryParse( this.Process.Added, out date ) )
                this.addedBox.Text = date.ToShortDateString();
            else
                this.addedBox.Text = this.Process.Added;

            if( DateTime.TryParse( this.Process.Updated, out date ) )
                this.updatedBox.Text = date.ToShortDateString();
            else
                this.updatedBox.Text = this.Process.Updated;
        }

        private void UpdateSource()
        {
            this.sourceBox.Item = this.Process.Source;
            this.sourceBox.IsConsumed = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeSource );
            this.sourceBox.IsPrimary = Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
        }

        private void UpdateTarget()
        {
            this.targetBox.Item = this.Process.Target;
            this.targetBox.IsConsumed = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeTarget );
            this.targetBox.IsPrimary = !Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
        }

        private void UpdateResult()
        {
            this.resultBox.Item = this.Process.Result;
        }

        private void OnSourceChanged( object sender, EventArgs e )
        {
            this.UpdateSource();
            ((MainForm)(this.MdiParent)).StatusText = String.Format( "Source changed to {0}", this.Process.Source );
            Twp.Utilities.Log.Debug( "[ProcessEditor.OnSourceChanged] New Source: {0}", this.Process.Source );
        }

        private void OnTargetChanged( object sender, EventArgs e )
        {
            this.UpdateTarget();
            ((MainForm)(this.MdiParent)).StatusText = String.Format( "Target changed to {0}", this.Process.Target );
            Twp.Utilities.Log.Debug( "[ProcessEditor.OnTargetChanged] New Target: {0}", this.Process.Target );
        }

        private void OnResultChanged( object sender, EventArgs e )
        {
            this.UpdateResult();
            ((MainForm)(this.MdiParent)).StatusText = String.Format( "Result changed to {0}", this.Process.Result );
            Twp.Utilities.Log.Debug( "[ProcessEditor.OnResultChanged] New Result: {0}", this.Process.Result );
        }

        private void AddSkill( Skill skill )
        {
            SkillEditBox seb = new SkillEditBox( skill, this.ActionManager );
            seb.Dock = DockStyle.Top;
            seb.Deleted += this.OnSkillDeleted;
            this.skillPanel.Controls.Add( seb );
        }

        private void OnAddSkillClicked( object sender, EventArgs e )
        {
            if( this.Process == null )
                return;

            this.ActionManager.RecordAction( new AddItemAction<Skill>( this.AddSkillAction, this.RemoveSkillAction, new Skill() ) );
        }

        private void OnSkillDeleted( object sender, EventArgs e )
        {
            SkillEditBox seb = sender as SkillEditBox;
            if( seb == null )
                return;

            this.ActionManager.RecordAction( new AddItemAction<Skill>( this.RemoveSkillAction, this.AddSkillAction, seb.Skill ) );
        }

        private void AddSkillAction( Skill skill )
        {
            this.Process.Skills.Add( skill );
            this.AddSkill( skill );
        }

        private void RemoveSkillAction( Skill skill )
        {
            this.Process.Skills.Remove( skill );
            foreach( Control control in this.skillPanel.Controls )
            {
                SkillEditBox seb = control as SkillEditBox;
                if( seb == null )
                    continue;

                if( seb.Skill == skill )
                {
                    this.skillPanel.Controls.Remove( control );
                }
            }
        }

        private void OnSkillsChanged( object sender, ListChangedEventArgs e )
        {
            string skillName;
            switch( e.ListChangedType )
            {
                case ListChangedType.ItemAdded:
                    skillName = Skill.GetName( this.Process.Skills[e.NewIndex].Key );
                    ((MainForm)(this.MdiParent)).StatusText = String.Format( "Skill {0} added...", skillName );
                    break;
                case ListChangedType.ItemChanged:
                    skillName = Skill.GetName( this.Process.Skills[e.NewIndex].Key );
                    ((MainForm)(this.MdiParent)).StatusText = String.Format( "Skill {0} changed to {1} with value {2}", e.NewIndex,
                                                                            skillName, this.Process.Skills[e.NewIndex].Value );
                    break;
                case ListChangedType.ItemDeleted:
                    ((MainForm)(this.MdiParent)).StatusText = "Skill deleted...";
                    break;
                case ListChangedType.Reset:
                    ((MainForm)(this.MdiParent)).StatusText = "Skill list reset...";
                    break;
                default:
                    break;
            }
        }

        private void ConsumeSourceMenuCheckedChanged( object sender, EventArgs e )
        {
            this.sourceBox.IsConsumed = this.consumeSourceToolStripMenuItem.Checked;
        }

        private void ConsumeSourceChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.RecordFlagsAction( ProcessFlags.ConsumeSource, this.sourceBox.IsConsumed );
        }

        private void ConsumeTargetMenuCheckedChanged( object sender, EventArgs e )
        {
            this.targetBox.IsConsumed = this.consumeTargetToolStripMenuItem.Checked;
        }

        private void ConsumeTargetChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.RecordFlagsAction( ProcessFlags.ConsumeTarget, this.targetBox.IsConsumed );
        }

        private void SetTargetAsPrimaryClicked( object sender, EventArgs e )
        {
            this.targetBox.IsPrimary = true;
        }

        private void SetSourceAsPrimaryClicked( object sender, EventArgs e )
        {
            this.sourceBox.IsPrimary = true;
        }

        private void SourceIsPrimaryChanged( object sender, EventArgs e )
        {
            Log.Debug( "[ProcessEditor.SourceIsPrimary] SiP: {0}, TiP: {1}", this.sourceBox.IsPrimary, this.targetBox.IsPrimary );
            this.targetBox.IsPrimary = !this.sourceBox.IsPrimary;
        }

        private void TargetIsPrimaryChanged( object sender, EventArgs e )
        {
            Log.Debug( "[ProcessEditor.TargetIsPrimary] SiP: {0}, TiP: {1}", this.sourceBox.IsPrimary, this.targetBox.IsPrimary );
            this.sourceBox.IsPrimary = !this.targetBox.IsPrimary;
            if( !this.IsUpdating )
                this.RecordFlagsAction( ProcessFlags.SourceIsPrimary, this.sourceBox.IsPrimary );
        }

        private void RecordFlagsAction( ProcessFlags flag, bool set )
        {
            ProcessFlags flags = this.Process.Flags;
            if( set )
                flags |= flag;
            else
                flags &= ~flag;

            this.ActionManager.RecordAction( new SetPropertyAction( this.Process, "Flags", flags ) );
        }

        private void MultiplierBoxValueChanged( object sender, EventArgs e )
        {
            if( !this.IsUpdating )
                this.ActionManager.RecordAction( new SetPropertyAction( this.Process, "Multiplier", this.multiplierBox.Value ) );
        }

        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            ((MainForm)(this.MdiParent)).StatusText = String.Format( "Property {0} changed...", e.PropertyName );
            this.BeginUpdate();
            switch( e.PropertyName )
            {
                case "Flags":
                    this.sourceBox.IsConsumed = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeSource );
                    this.targetBox.IsConsumed = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeTarget );
                    this.sourceBox.IsPrimary = Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
                    this.targetBox.IsPrimary = !Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
                    this.consumeSourceToolStripMenuItem.Checked = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeSource );
                    this.consumeTargetToolStripMenuItem.Checked = Flag.IsSet( this.Process.Flags, ProcessFlags.ConsumeTarget );
                    this.setSourceAsPrimaryObjectToolStripMenuItem.Enabled = !Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
                    this.setTargetAsPrimaryObjectToolStripMenuItem.Enabled = Flag.IsSet( this.Process.Flags, ProcessFlags.SourceIsPrimary );
                    break;
                case "Multiplier":
                    this.multiplierBox.Value = this.Process.Multiplier;
                    break;
                case "Note":
                    this.noteBox.Text = this.Process.Note;
                    break;
            }
            this.EndUpdate();
        }

        private void OnEditNoteClicked( object sender, EventArgs e )
        {
//            using( Twp.Controls.HtmlEditor editor = new Twp.Controls.HtmlEditor() )
//            {
//                editor.Html = this.Process.Note;
//                if( editor.ShowDialog() == System.Windows.Forms.DialogResult.OK )
//                {
//                    this.ActionManager.RecordAction( new SetPropertyAction( this.Process, "Note", editor.Html ) );
//                }
//            }
        }

        #endregion

        #region Drag&Drop

        private void OnSourceBoxDropped( object sender, DragEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( "Formats: " + String.Join( ", ", e.Data.GetFormats() ), "[ProcessForm.DragDrop]" );
            ItemList items = (ItemList) e.Data.GetData( typeof( ItemList ) );
            System.Diagnostics.Debug.WriteLineIf( items == null, "e.Data.GetData returns null!", "[ProcessForm.DragDrop]" );
            if( items != null || items.Count < 1 )
            {
                System.Diagnostics.Debug.WriteLine( "Items: " + items.Count.ToString(), "[ProcessForm.DragDrop]" );
                if( this.sourceBox.Item == items[0] )
                    return;
                else if( this.targetBox.Item == items[0] && !Flag.IsSet( e.KeyState, KeyStates.Shift ) )
                    this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Source", "Target" ) );
                else if( this.resultBox.Item == items[0] && !Flag.IsSet( e.KeyState, KeyStates.Shift ) )
                    this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Source", "Result" ) );
                else
                    this.ActionManager.RecordAction( new SetPropertyAction( this.Process, "Source", items[0]) );
            }
        }

        private void OnTargetBoxDropped( object sender, DragEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( "Formats: " + String.Join( ", ", e.Data.GetFormats() ), "[ProcessForm.DragDrop]" );
            ItemList items = (ItemList) e.Data.GetData( typeof( ItemList ) );
            System.Diagnostics.Debug.WriteLineIf( items == null, "e.Data.GetData returns null!", "[ProcessForm.DragDrop]" );
            if( items != null || items.Count < 1 )
            {
                System.Diagnostics.Debug.WriteLine( "Items: " + items.Count.ToString(), "[ProcessForm.DragDrop]" );
                if( this.sourceBox.Item == items[0] && !Flag.IsSet( e.KeyState, KeyStates.Shift ) )
                    this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Target", "Source" ) );
                else if( this.targetBox.Item == items[0] )
                    return;
                else if( this.resultBox.Item == items[0] && !Flag.IsSet( e.KeyState, KeyStates.Shift ) )
                    this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Target", "Result" ) );
                else
                    this.ActionManager.RecordAction( new SetPropertyAction( this.Process, "Target", items[0]) );
            }
        }

        private void OnResultBoxDropped( object sender, DragEventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( "Formats: " + String.Join( ", ", e.Data.GetFormats() ), "[ProcessForm.DragDrop]" );
            ItemList items = (ItemList) e.Data.GetData( typeof( ItemList ) );
            System.Diagnostics.Debug.WriteLineIf( items == null, "e.Data.GetData returns null!", "[ProcessForm.DragDrop]" );
            if( items != null || items.Count < 1 )
            {
                System.Diagnostics.Debug.WriteLine( "Items: " + items.Count.ToString(), "[ProcessForm.DragDrop]" );
                if( this.sourceBox.Item == items[0] && !Flag.IsSet( e.KeyState, KeyStates.Shift ) )
                    this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Source", "Result" ) );
                else if( this.targetBox.Item == items[0] && !Flag.IsSet( e.KeyState, KeyStates.Shift ) )
                    this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Target", "Result" ) );
                else if( this.resultBox.Item == items[0] )
                    return;
                else
                    this.ActionManager.RecordAction( new SetPropertyAction( this.Process, "Result", items[0] ) );
            }
        }

        #endregion

        #region ContextMenu

        private void FindProcess( ProcItem item )
        {
            ProcessList processes = Process.Search( item, false );
            if( processes.Contains( this.Process ) )
                processes.Remove( this.Process );
            if( processes.Count == 0 )
            {
                string text = String.Format( "No processes were found where {0} is a result. Try creating one.", item );
                MessageBox.Show( text, "No processes found", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
            else
            {
                this.OnOpenEditors( new ItemsEventArgs( processes.ToArray() ) );
            }
        }

        private void FindTradeskills( Process result )
        {
            TradeskillList tradeskills = Tradeskill.Search( result );
            if( tradeskills.Count == 0 )
            {
                string text = String.Format( "No tradeskills were found where {0} is a result. Try to create one.", result );
                MessageBox.Show( text, "No tradeskills found", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
            else
            {
                this.OnOpenEditors( new ItemsEventArgs( tradeskills.ToArray() ) );
            }
        }

        private void OnFindSourceClicked( object sender, EventArgs e )
        {
            this.FindProcess( this.Process.Source );
        }

        private void OnCreateSourceClicked( object sender, EventArgs e )
        {
            Process process = new Process();
            process.Result = this.Process.Source;
            this.OnCreateEditor( new DataEventArgs( process ) );
        }

        private void OnFindTargetClicked( object sender, EventArgs e )
        {
            this.FindProcess( this.Process.Target );
        }

        private void OnCreateTargetClicked( object sender, EventArgs e )
        {
            Process process = new Process();
            process.Result = this.Process.Target;
            this.OnCreateEditor( new DataEventArgs( process ) );
        }

        private void OnFindResultClicked( object sender, EventArgs e )
        {
            this.FindProcess( this.Process.Result );
        }

        private void OnFindTradeskillsClicked( object sender, EventArgs e )
        {
            this.FindTradeskills( this.Process );
        }

        private void OnCreateTradeskillClicked( object sender, EventArgs e )
        {
            ProcessList processes = new ProcessList();
            processes.Add( this.Process );
            this.OnCreateEditor( new DataEventArgs( new Tradeskill( processes ) ) );
        }

        private void OnSwapSourceTargetClicked( object sender, EventArgs e )
        {
            if( this.Process == null )
                return;

            this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Source", "Target" ) );
            ((MainForm)(this.MdiParent)).StatusText = "Source and Target swapped...";
        }

        private void OnSwapTargetResultClicked( object sender, EventArgs e )
        {
            if( this.Process == null )
                return;

            this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Target", "Result" ) );
            ((MainForm)(this.MdiParent)).StatusText = "Target and Result swapped...";
        }

        private void OnSwapSourceResultClicked( object sender, EventArgs e )
        {
            if( this.Process == null )
                return;

            this.ActionManager.RecordAction( new SwapItemsAction( this.Process, "Source", "Result" ) );
            ((MainForm)(this.MdiParent)).StatusText = "Source and Result swapped...";
        }

        private void OnCreateCopyClicked( object sender, EventArgs e )
        {
            if( this.Process == null )
                return;

            this.OnOpenEditor( new DataEventArgs( new Process( this.Process ) ) );
            ((MainForm)(this.MdiParent)).StatusText = "Process copied...";
        }

        #endregion
    }
}
