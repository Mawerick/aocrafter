﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using AOCrafter.Core;

namespace AOCrafter.Admin
{
    #region Delegates

    public delegate void DataEventHandler( object sender, DataEventArgs e );
    public delegate void ItemsEventHandler( object sender, ItemsEventArgs e );

    #endregion

    /// <summary>
    /// Description of DataEventArgs.
    /// </summary>
    public class DataEventArgs : EventArgs
    {
        public DataEventArgs( ICrafterData data )
        {
            this.data = data;
        }

        private ICrafterData data;

        /// <summary>
        ///
        /// </summary>
        public ICrafterData Data
        {
            get { return this.data; }
        }
    }

    /// <summary>
    /// Description of ItemsEventArgs.
    /// </summary>
    public class ItemsEventArgs : EventArgs
    {
        public ItemsEventArgs( ICrafterData[] items )
        {
            this.items = items;
        }

        private ICrafterData[] items;

        /// <summary>
        ///
        /// </summary>
        public ICrafterData[] Items
        {
            get { return this.items; }
        }
    }
}
