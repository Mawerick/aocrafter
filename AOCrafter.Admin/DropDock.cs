﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Data.AO;
using Twp.Utilities;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class DropDock : DockContent
    {
        public DropDock()
        {
            InitializeComponent();
        }

        public void AddItems( ItemList items )
        {
            if( items == null )
                return;

            foreach( ProcItem item in items )
                this.AddItem( item );
        }

        private void OnClearListClicked( object sender, EventArgs e )
        {
            this.listView.Items.Clear();
        }

        private void OnRemoveItemClicked( object sender, EventArgs e )
        {
            foreach( ListViewItem item in this.listView.SelectedItems )
                this.listView.Items.Remove( item );
        }

#region Drag & Drop

        private bool mouseButtonDown = false;

        private void OnMouseDown( object sender, MouseEventArgs e )
        {
            if( e.Clicks > 1 )
                return;

            if( e.Button == MouseButtons.Left )
                this.mouseButtonDown = true;
        }

        private void OnMouseMove( object sender, MouseEventArgs e )
        {
            if( this.mouseButtonDown == false )
                return;

            if( e.Clicks > 1 )
                return;

            if( e.Button == MouseButtons.Left )
            {
                ItemList items = new ItemList();
                foreach( ListViewItem listItem in this.listView.SelectedItems )
                {
                    items.Add( (ProcItem) listItem.Tag );
                }
                this.DoDragDrop( items, DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

        private void OnDragEnter( object sender, DragEventArgs e )
        {
            Log.Debug( "[DropBox.DragEnter] Formats: {0}", String.Join( ", ", e.Data.GetFormats() ) );
            if( e.Data.GetDataPresent( typeof( ItemList ) ) )
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void OnDragDrop( object sender, DragEventArgs e )
        {
            Log.Debug( "[DropBox.DragDrop] Formats: {0}", String.Join( ", ", e.Data.GetFormats() ) );
            ItemList items = (ItemList) e.Data.GetData( typeof( ItemList ) );
            if( items != null )
            {
                Log.Debug( "[DropBox.DragDrop] Items: {0}", items.Count );
                foreach( ProcItem item in items )
                    this.AddItem( item );
            }
        }

        private void AddItem( ProcItem item )
        {
            Log.Debug( "[DropBox.listView_Add] Item: {0}", item.Id );
            if( this.listView.Items.ContainsKey( item.Id.ToString() ) )
                return;

            Item tmp = item.GetNearest( 100 );
            if( tmp == null )
                return;

            AOItem aoitem = tmp.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );

            if( this.icons.Images.ContainsKey( aoitem.Icon.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( aoitem.Icon );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }

            ListViewItem listItem = this.listView.Items.Add( item.Id.ToString(), aoitem.Name, aoitem.Icon.ToString() );
            listItem.SubItems.Add( aoitem.Description );
            listItem.Tag = item;
        }

#endregion
    }
}
