﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

using AOCrafter.Core;
using AOCrafter.Core.Controls;
using Twp.Data.AO;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class ItemsDock : DockContent
    {
        public ItemsDock()
        {
            InitializeComponent();
        }

        public void ShowAll()
        {
            this.OnSearch( this, new SearchEventArgs( new string[] { "ALL" } ) );
        }

        private void OnSearch( object sender, SearchEventArgs e )
        {
            this.resultList.Items.Clear();
            this.progressIndicator.Show();
            this.progressIndicator.Start();
            using( BackgroundWorker bw = new BackgroundWorker() )
            {
                bw.DoWork += new DoWorkEventHandler( DoSearch );
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler( OnSearchCompleted );
                bw.RunWorkerAsync( e.Words );
            }
        }

        private void DoSearch( object sender, DoWorkEventArgs e )
        {
            string[] args = e.Argument as string[];
            if( args == null || args.Length == 0 )
                return;
#if DEBUG
            else if( args[0].ToUpperInvariant() == "ALL" )
                e.Result = ProcItem.GetAll();
#endif
            else if( args[0].ToUpperInvariant() == "UNUSED" )
                e.Result = ProcItem.FindUnused();
            else
                e.Result = ProcItem.Search( args );
        }

        private void OnSearchCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            ItemList items = e.Result as ItemList;

            if( items == null || items.Count == 0 )
            {
                this.countLabel.Text = "No items found.";
            }
            else
            {
                this.resultList.BeginUpdate();
                foreach( ProcItem item in items )
                {
                    this.listView_Add( item );
                    Application.DoEvents();
                }
                this.countLabel.Text = "Items found: " + items.Count.ToString();
                this.resultList.EndUpdate();
            }
            this.progressIndicator.Stop();
            this.progressIndicator.Hide();
        }

        private void listView_Add( ProcItem item )
        {
            //Log.Debug( "[ItemSearchBox.listView_Add] Id: {0}", item.Id );
            Item tmp = item.GetNearest( item.HighQL );
            if( tmp == null )
                return;
            AOItem aoitem = tmp.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
            this.icons_Add( aoitem.Icon );

            ListViewItem listItem = new ListViewItem();
            listItem.SubItems.Add( item.Id.ToString() );
            listItem.SubItems.Add( aoitem.Name );
            if( item.LowQL == item.HighQL )
                listItem.SubItems.Add( item.LowQL.ToString() );
            else
                listItem.SubItems.Add( String.Format( "{0} - {1}", item.LowQL, item.HighQL ) );
            listItem.ImageKey = aoitem.Icon.ToString();
            listItem.Tag = item;

            this.resultList.Items.Add( listItem );
        }

        private void icons_Add( int id )
        {
            //Log.Debug( "[ItemSearchBox.icons_Add] Id: {0}", id );
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
        }

        #region Drag & Drop

        private bool mouseButtonDown = false;

        private void resultList_MouseDown( object sender, MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
                this.mouseButtonDown = true;
        }

        private void resultList_MouseMove( object sender, MouseEventArgs e )
        {
            if( this.mouseButtonDown == false )
                return;

            if( e.Button == MouseButtons.Left )
            {
                ItemList items = new ItemList();
                foreach( ListViewItem listItem in this.resultList.SelectedItems )
                {
                    items.Add( (ProcItem) listItem.Tag );
                }
                this.resultList.DoDragDrop( items, DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

        #endregion

        [Category( "Behavior" )]
        [Description( "Occurs when one or more items are activated in the list, after an Item search" )]
        public event ItemsEventHandler OpenItems;

        [Category( "Behavior" )]
        [Description( "Occurs when the \"Create Process\" context menu item is activated, after an Item search" )]
        public event DataEventHandler CreateProcess;

        protected virtual void OnOpenItems( ItemsEventArgs e )
        {
            if( this.OpenItems != null )
                this.OpenItems( this, e );
        }

        protected virtual void OnCreateProcess( DataEventArgs e )
        {
            if( this.CreateProcess != null )
                this.CreateProcess( this, e );
        }

        private void OnItemActivated( object sender, EventArgs e )
        {
            this.OpenItem();
        }

        private void OnOpenItemClicked( object sender, EventArgs e )
        {
            this.OpenItem();
        }

        private void OpenItem()
        {
            ItemList items = new ItemList();
            foreach( ListViewItem listItem in this.resultList.SelectedItems )
            {
                items.Add( (ProcItem) listItem.Tag );
            }
            this.OnOpenItems( new ItemsEventArgs( items.ToArray() ) );
        }

        private void OnCreateProcessClicked( object sender, EventArgs e )
        {
            if( this.resultList.SelectedItems.Count > 1 )
            {
                DialogResult dr = MessageBox.Show( "Do you wish to create a Process for every item selected? Each item will be set as the result of the process.",
                                                  "Multiple items selected.", MessageBoxButtons.YesNo, MessageBoxIcon.Question );
                if( dr == DialogResult.Yes )
                {
                    foreach( ListViewItem listItem in this.resultList.SelectedItems )
                    {
                        Process process = new Process();
                        process.Result = (ProcItem) listItem.Tag;
                        this.OnCreateProcess( new DataEventArgs( process ) );
                    }
                }
            }
            else if( this.resultList.SelectedItems.Count == 1 )
            {
                ListViewItem listItem = this.resultList.SelectedItems[0];
                Process process = new Process();
                process.Result = (ProcItem) listItem.Tag;
                this.OnCreateProcess( new DataEventArgs( process ) );
            }
        }
    }
}
