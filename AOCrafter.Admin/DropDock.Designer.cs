﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class DropDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView = new System.Windows.Forms.ListView();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearListMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.removeItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.AllowDrop = true;
            this.listView.ContextMenuStrip = this.contextMenu;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.LargeImageList = this.icons;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(584, 212);
            this.listView.SmallImageList = this.icons;
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDragDrop);
            this.listView.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            this.listView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.listView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnMouseMove);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearListMenuItem,
            this.removeItemToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(196, 70);
            // 
            // clearListMenuItem
            // 
            this.clearListMenuItem.Name = "clearListMenuItem";
            this.clearListMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Delete)));
            this.clearListMenuItem.Size = new System.Drawing.Size(195, 22);
            this.clearListMenuItem.Text = "Clear the list";
            this.clearListMenuItem.Click += new System.EventHandler(this.OnClearListClicked);
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.icons.ImageSize = new System.Drawing.Size(48, 48);
            this.icons.TransparentColor = System.Drawing.Color.Lime;
            // 
            // removeItemToolStripMenuItem
            // 
            this.removeItemToolStripMenuItem.Name = "removeItemToolStripMenuItem";
            this.removeItemToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.removeItemToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.removeItemToolStripMenuItem.Text = "Remove item";
            this.removeItemToolStripMenuItem.Click += new System.EventHandler(this.OnRemoveItemClicked);
            // 
            // DropDock
            // 
            this.ClientSize = new System.Drawing.Size(584, 212);
            this.Controls.Add(this.listView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideOnClose = true;
            this.Name = "DropDock";
            this.Text = "Drop box";
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        
        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ImageList icons;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem clearListMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeItemToolStripMenuItem;
    }
}
