﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Client
{
    partial class TradeskillBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.settingsButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.line1 = new Twp.Controls.Line();
            this.tabControl = new Twp.Controls.TabControl();
            this.processPage = new System.Windows.Forms.TabPage();
            this.processBox = new AOCrafter.Core.Controls.ProcessBox();
            this.summaryPage = new System.Windows.Forms.TabPage();
            this.summaryBox = new AOCrafter.Client.SummaryBox();
            this.selectionBox = new AOCrafter.Client.SelectionBox();
            this.tabControl.SuspendLayout();
            this.processPage.SuspendLayout();
            this.summaryPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsButton
            // 
            this.settingsButton.Location = new System.Drawing.Point(0, 0);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(75, 23);
            this.settingsButton.TabIndex = 0;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(0, 0);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 0;
            // 
            // line1
            // 
            this.line1.Location = new System.Drawing.Point(0, 0);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(0, 10);
            this.line1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.processPage);
            this.tabControl.Controls.Add(this.summaryPage);
            this.tabControl.ItemSize = new System.Drawing.Size(0, 17);
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(444, 444);
            this.tabControl.TabIndex = 0;
            // 
            // processPage
            // 
            this.processPage.BackColor = System.Drawing.SystemColors.Control;
            this.processPage.Controls.Add(this.processBox);
            this.processPage.Location = new System.Drawing.Point(4, 21);
            this.processPage.Name = "processPage";
            this.processPage.Size = new System.Drawing.Size(436, 419);
            this.processPage.TabIndex = 0;
            this.processPage.Text = "Processes";
            // 
            // processBox
            // 
            this.processBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processBox.Location = new System.Drawing.Point(0, 0);
            this.processBox.Name = "processBox";
            this.processBox.Padding = new System.Windows.Forms.Padding(2, 4, 2, 2);
            this.processBox.Size = new System.Drawing.Size(436, 419);
            this.processBox.TabIndex = 0;
            this.processBox.LinkClicked += new AOCrafter.Core.Controls.LinkedTradeskillEventHandler(this.OnLinkClicked);
            this.processBox.SelectionChanged += new System.EventHandler(this.OnSelectionChanged);
            // 
            // summaryPage
            // 
            this.summaryPage.BackColor = System.Drawing.SystemColors.Control;
            this.summaryPage.Controls.Add(this.summaryBox);
            this.summaryPage.Location = new System.Drawing.Point(4, 21);
            this.summaryPage.Name = "summaryPage";
            this.summaryPage.Padding = new System.Windows.Forms.Padding(3);
            this.summaryPage.Size = new System.Drawing.Size(436, 419);
            this.summaryPage.TabIndex = 1;
            this.summaryPage.Text = "Summary";
            // 
            // summaryBox
            // 
            this.summaryBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.summaryBox.Location = new System.Drawing.Point(3, 3);
            this.summaryBox.Name = "summaryBox";
            this.summaryBox.Size = new System.Drawing.Size(430, 413);
            this.summaryBox.TabIndex = 0;
            // 
            // selectionBox
            // 
            this.selectionBox.Location = new System.Drawing.Point(0, 0);
            this.selectionBox.Name = "selectionBox";
            this.selectionBox.Size = new System.Drawing.Size(390, 76);
            this.selectionBox.TabIndex = 0;
            this.selectionBox.Tradeskill = null;
            // 
            // TradeskillBox
            // 
            this.Controls.Add(this.tabControl);
            this.Name = "TradeskillBox";
            this.Size = new System.Drawing.Size(450, 450);
            this.tabControl.ResumeLayout(false);
            this.processPage.ResumeLayout(false);
            this.summaryPage.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private Twp.Controls.TabControl tabControl;
        private System.Windows.Forms.TabPage processPage;
        private System.Windows.Forms.TabPage summaryPage;
        private SummaryBox summaryBox;
        private AOCrafter.Core.Controls.ProcessBox processBox;
        private Twp.Controls.Line line1;
        private SelectionBox selectionBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button settingsButton;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

