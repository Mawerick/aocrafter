﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.ComponentModel;
using System.Windows.Forms;
using AOCrafter.Client.Data;
using AOCrafter.Core;
using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Client
{
    public partial class SummaryBox : UserControl
    {
        public SummaryBox()
        {
            InitializeComponent();
        }

        private Tradeskill tradeskill;
        private SummaryList<ProcItem> consumed = new SummaryList<ProcItem>();
        private SummaryList<ProcItem> tools = new SummaryList<ProcItem>();
        private SummaryList<SkillName> skills = new SummaryList<SkillName>();

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Tradeskill Tradeskill
        {
            get { return this.tradeskill; }
            set { this.tradeskill = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal SummaryList<ProcItem> Consumed
        {
            get { return this.consumed; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal SummaryList<ProcItem> Tools
        {
            get { return this.tools; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal SummaryList<SkillName> Skills
        {
            get { return this.skills; }
        }

        public void Reset()
        {
            this.consumed.Clear();
            this.tools.Clear();
            this.skills.Clear();
        }

        public void UpdateSummary()
        {
            this.Reset();
            ItemList results = new ItemList();
            foreach( ProcessNode node in this.tradeskill.Nodes.EnumProcesses() )
            {
                Log.Debug( "[SummaryBox.UpdateSummary] Result: {0}", node.Process.Result );
                results.Add( node.Process.Result );
            }

            foreach( ProcessNode node in this.tradeskill.Nodes.EnumProcesses() )
            {
                Log.Debug( "[SummaryBox.UpdateSummary] Source: {0}", node.Process.Source );
                if( !results.Contains( node.Process.Source ) )
                {
                    Log.Debug( "[SummaryBox.UpdateSummary] Consumed: {0}", Flag.IsSet( node.Process.Flags, ProcessFlags.ConsumeSource ) );
                    if( Flag.IsSet( node.Process.Flags, ProcessFlags.ConsumeSource ) )
                        this.consumed.Add( node.Process.Source, node.Process.FinalSource.QL, node.RealCount );
                    else
                        this.tools.Add( node.Process.Source, node.Process.FinalSource.QL, node.RealCount );
                }
                Log.Debug( "[SummaryBox.UpdateSummary] Target: {0}", node.Process.Target );
                if( !results.Contains( node.Process.Target ) )
                {
                    Log.Debug( "[SummaryBox.UpdateSummary] Consumed: {0}", Flag.IsSet( node.Process.Flags, ProcessFlags.ConsumeTarget ) );
                    if( Flag.IsSet( node.Process.Flags, ProcessFlags.ConsumeTarget ) )
                        this.consumed.Add( node.Process.Target, node.Process.FinalTarget.QL, node.RealCount );
                    else
                        this.tools.Add( node.Process.Target, node.Process.FinalTarget.QL, node.RealCount );
                }
                foreach( Skill skill in node.Process.Skills )
                {
                    this.skills.Add( skill.Key, skill.FinalValue, 1 );
                }
            }

            this.UpdateConsumedView();
            this.UpdateToolsView();
            this.UpdateSkillsView();
        }

        private void UpdateConsumedView()
        {
            this.consumedView.BeginUpdate();
            this.consumedView.Items.Clear();
            foreach( SummaryNode<ProcItem> node in this.consumed )
            {
                AOItem aoitem = GetItem( node.Item, node.Value );
                if( aoitem == null )
                    continue;
                ListViewItem item = new ListViewItem();
                item.Text = aoitem.Name;
                item.SubItems.Add( aoitem.Ql.ToString() );
                item.SubItems.Add( node.Count.ToString() );
                item.ImageIndex = this.AddIcon( aoitem.Icon );
                this.consumedView.Items.Add( item );
            }
            this.consumedView.EndUpdate();
        }

        private void UpdateToolsView()
        {
            this.toolsView.BeginUpdate();
            this.toolsView.Items.Clear();
            foreach( SummaryNode<ProcItem> node in this.tools )
            {
                AOItem aoitem = GetItem( node.Item, node.Value );
                if( aoitem == null )
                    continue;
                ListViewItem item = new ListViewItem();
                item.Text = aoitem.Name;
                item.SubItems.Add( aoitem.Ql.ToString() );
                item.ImageIndex = this.AddIcon( aoitem.Icon );
                this.toolsView.Items.Add( item );
            }
            this.toolsView.EndUpdate();
        }

        private static AOItem GetItem( ProcItem procItem, int ql )
        {
            Item item = procItem.GetNearest( ql );
            if( item == null )
                return null;
            return item.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
        }

        private void UpdateSkillsView()
        {
            this.skillsView.BeginUpdate();
            this.skillsView.Items.Clear();
            foreach( SummaryNode<SkillName> node in this.skills )
            {
                ListViewItem item = new ListViewItem();
                item.Text = Skill.GetName( node.Item );
                item.SubItems.Add( node.Value.ToString() );
                item.ImageIndex = this.AddIcon( Skill.GetIcon( node.Item ) );
                this.skillsView.Items.Add( item );
            }
            this.skillsView.EndUpdate();
        }

        private int AddIcon( int id )
        {
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
            return this.icons.Images.IndexOfKey( id.ToString() );
        }
    }
}
