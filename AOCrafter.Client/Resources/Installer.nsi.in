﻿;
;  Copyright (C) Mawerick, WrongPlace.Net 2012
;
;  This program is free software: you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

;---------------------------------------
;include Modern UI

  !include MUI2.nsh

;---------------------------------------
;defined constants

  !define UNINST_REGKEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\@PRODUCT@"

;---------------------------------------
;General

  ;Name and file
  Name "@PRODUCT@"
  OutFile "D:\\Workspace\\_Releases\\@PRODUCT@\\@PRODUCT@-@VERSION_SHORT@.exe"
  BrandingText "@COMPANY@ | @URL@"

  ;Default install folder
  InstallDir $PROGRAMFILES\@COMPANY@\@PRODUCT@

  ;Get install folder from registry if available
  InstallDirRegKey HKLM "Software\@COMPANY@\@PRODUCT@" "InstallPath"

  ;Request app priveleges (Vista)
  RequestExecutionLevel admin

  SetCompressor lzma

;---------------------------------------
;Variables

  Var StartMenuFolder
  Var DATADIR

;--------------------------------
;Version Information

  VIProductVersion "@VERSION_SHORT@.0"
  VIAddVersionKey "ProductName" "@PRODUCT@"
  VIAddVersionKey "Comments" "@DESCRIPTION@"
  VIAddVersionKey "CompanyName" "@COMPANY@"
  VIAddVersionKey "LegalCopyright" "@COPYRIGHT@"
  VIAddVersionKey "FileDescription" "@PRODUCT@ Installer"
  VIAddVersionKey "FileVersion" "@VERSION_SHORT@.0"

;---------------------------------------
;Interface settings

  !define MUI_HEADERIMAGE
  ;!define MUI_HEADERIMAGE_BITMAP "installer.bmp"
  !define MUI_ABORTWARNING
  !define MUI_FINISHPAGE_NOAUTOCLOSE
  !define MUI_UNFINISHPAGE_NOAUTOCLOSE

;---------------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "@LICENSE@"
  !insertmacro MUI_PAGE_DIRECTORY

  ;Data Folder Page Configuration
  !define MUI_PAGE_HEADER_SUBTEXT "Choose the folder in which to install the database."
  !define MUI_DIRECTORYPAGE_TEXT_TOP "The installer will install the database in the following folder. To install in a differenct folder, click Browse and select another folder. Click Next to continue."
  !define MUI_DIRECTORYPAGE_VARIABLE $DATADIR
  !define MUI_PAGE_CUSTOMFUNCTION_PRE CheckDataDir
  !insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\@COMPANY@\@PRODUCT@"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  !define MUI_STARTMENUPAGE_DEFAULTFOLDER "@COMPANY@\@PRODUCT@"
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;---------------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;---------------------------------------
;Functions

Function .onInit
  SetShellVarContext all
  StrCpy $DATADIR "$APPDATA\@COMPANY@\@PRODUCT@"
FunctionEnd

Function un.onInit
  SetShellVarContext all
  IfFileExists "$APPDATA\@COMPANY@\@PRODUCT@\@PRODUCT@.ini" 0 SetDefault
  ClearErrors
  ReadINIStr $DATADIR "$APPDATA\@COMPANY@\@PRODUCT@\@PRODUCT@.ini" "General" "DataPath"
  IfErrors SetDefault
    Return

SetDefault:
  StrCpy $DATADIR "$APPDATA\@COMPANY@\@PRODUCT@"
FunctionEnd

Function CheckDataDir
  StrCmp $INSTDIR "$PROGRAMFILES\@COMPANY@\@PRODUCT@" 0 +2
    Return
  StrCpy $DATADIR "$INSTDIR\Data"
FunctionEnd

Function UpdateINI
  SetShellVarContext current
  IfFileExists "$APPDATA\@COMPANY@\@PRODUCT@\@PRODUCT@.ini" 0 UpdateIni

  ClearErrors
  ReadINIStr $0 "$APPDATA\@COMPANY@\@PRODUCT@\@PRODUCT@.ini" "General" "DataPath"
  IfErrors UpdateIni

  StrCmp $0 $DATADIR IniOk

UpdateIni:
  WriteINIStr "$APPDATA\@COMPANY@\@PRODUCT@\@PRODUCT@.ini" "General" "DataPath" $DATADIR

IniOk:  
FunctionEnd

Function RemoveOldInstall
  ReadRegStr $0 HKLM "SOFTWARE\@COMPANY@\@PRODUCT@" "InstallPath"

  IfFileExists "$0\uninstall.exe" 0 +2
    ExecWait '"$0\uninstall.exe" /S _?=$0'

  ReadRegStr $0 HKLM "SOFTWARE\@PRODUCT@" "InstallPath"

  IfFileExists "$0\uninstall.exe" 0 +2
    ExecWait '"$0\uninstall.exe" /S _?=$0'

FunctionEnd

;---------------------------------------
;Installer sections
Section "Program Files"
  SectionIn 1 2 RO

  Call RemoveOldInstall
  Call UpdateINI

  SetOutPath $INSTDIR
  @FILES@

  SetOutPath $DATADIR
  @FILES_DATA@

  ;Store install folder
  WriteRegStr HKLM "SOFTWARE\@COMPANY@\@PRODUCT@" "InstallPath" "$INSTDIR"

  ;Create uninstaller
  WriteUninstaller "$2\uninstall.exe"
  WriteRegStr HKLM "${UNINST_REGKEY}" "DisplayName" "$(^Name)"
  WriteRegStr HKLM "${UNINST_REGKEY}" "DisplayIcon" "$INSTDIR\@PRODUCT@.exe"
  WriteRegStr HKLM "${UNINST_REGKEY}" "DisplayVersion" "@VERSION@"
  WriteRegStr HKLM "${UNINST_REGKEY}" "UninstallString" "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "${UNINST_REGKEY}" "URLInfoAbout" "@URL@"
  WriteRegStr HKLM "${UNINST_REGKEY}" "Publisher" "@COMPANY@"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application

  ;Create shortcuts
  SetShellVarContext all
  CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
  @SHORTCUTS@
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

;---------------------------------------
;Uninstaller section
Section "Uninstall"

  ;Delete files and folders
  Delete "$INSTDIR\*.*"
  RMDir "$INSTDIR"

  Delete "$DATADIR\*.*"
  RMDir "$DATADIR"

  ;Delete startmenu folder
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  RMDir /r "$SMPROGRAMS\$StartMenuFolder"

  DeleteRegKey HKLM "Software\@COMPANY@\@PRODUCT@"
  DeleteRegKey /ifempty HKLM "Software\@COMPANY@"
  DeleteRegKey HKLM "${UNINST_REGKEY}"

SectionEnd

; vim: ts=2 sw=2 et:
