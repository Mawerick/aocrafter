﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Controls;
using Twp.Utilities;

namespace AOCrafter.Client
{
    public partial class TradeskillBox : UserControl
    {
        public TradeskillBox()
        {
            InitializeComponent();

            this.progressForm.BackColor = Color.FromArgb( 50, 50, 60 );
            this.progressForm.ForeColor = Color.FromArgb( 99, 173, 99 );
            this.progressForm.ProgressColor = Color.FromArgb( 99, 173, 99 );
        }

        private SimpleProgressForm progressForm = new SimpleProgressForm();
        private Tradeskill tradeskill = null;
        private int ql = 0;
        private bool max = false;

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int SplitterDistance
        {
            get { return this.processBox.SplitterDistance; }
            set { this.processBox.SplitterDistance = value; }
        }

        public void SetTradeskill( Tradeskill tradeskill, int ql, bool max )
        {
            this.tradeskill = tradeskill;
            this.ql = ql;
            this.max = max;
            this.TradeskillChanged();
        }

        private void TradeskillChanged()
        {
            if( this.tradeskill == null )
            {
                this.processBox.Tradeskill = null;
                this.summaryBox.Tradeskill = null;
                return;
            }

            this.progressForm.Label = String.Format( "Calculating tradeskill:\n{0}", this.tradeskill );
            this.progressForm.Start();
            this.progressForm.Show( this.FindForm() );

            this.processBox.Tradeskill = this.tradeskill;
            this.summaryBox.Tradeskill = this.tradeskill;

            if( this.max )
                this.tradeskill.Calculate( Program.MySkills );
            else
                this.tradeskill.Calculate( this.ql );
            Application.DoEvents();

            this.processBox.UpdateProcess();
            this.summaryBox.UpdateSummary();

            this.progressForm.Stop();
            this.progressForm.Hide();
        }

        private void OnLinkClicked( object sender, AOCrafter.Core.Controls.LinkedTradeskillEventArgs e )
        {
            Program.OpenLinkedTradeskill( e.Tradeskill, e.QL );
        }

        private void OnSelectionChanged( object sender, EventArgs e )
        {
            this.summaryBox.UpdateSummary();
        }
    }
}
