﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using AOCrafter.Core;
using Twp.Controls;
using Twp.Utilities;

namespace AOCrafter.Client.Data
{
    public class MySkills : SkillsList
    {
        public MySkills() : base()
        {
            this.doc = new IniDocument( "MySkills.ini", Path.GetAppDataPath( "WrongPlace.Net", "AOCrafter" ) );
        }

        private IniDocument doc;

        public void Load()
        {
			try
			{
				this.doc.Read();
			}
			catch( IniParseException ex )
			{
				Log.Warning( ex.Message );
				Log.Debug( "[MySkills.Load] {0}", ex.Line );
			}
			if( !this.doc.Sections.ContainsKey( "MySkills" ) )
			    return;

			try
			{
    			foreach( KeyValuePair<string, string> line in this.doc["MySkills"] )
    			{
    			    Skill skill = new Skill();
    			    skill.Key = (SkillName) Enum.Parse( typeof( SkillName ), line.Key );
    			    skill.Value = Decimal.Parse( line.Value );
    				Log.Debug( "[MySkills.Load] {0} = {1}", skill.Key, skill.Value );
    				this.Add( skill );
    			}
			}
			catch( Exception ex )
			{
			    ExceptionDialog.Show( ex );
			}
			Log.Debug( "[MySkills.Load] Skills: {0}", this.Count );
        }

        public void Save()
        {
			Log.Debug( "[MySkills.Save] Skills: {0}", this.Count );
            this.doc.Sections.Clear();
            IniSection section = new IniSection();
            foreach( Skill skill in this )
            {
                Log.Debug( "[MySkills.Save] Skill: {0} = {1}", skill.Key, skill.Value );
                section.Add( skill.Key.ToString(), skill.Value.ToString() );
            }
            this.doc.Sections.Add( "MySkills", section );
			try
			{
				this.doc.Write();
			}
			catch( Exception ex )
			{
				ExceptionDialog.Show( ex );
			}
        }
    }
}
