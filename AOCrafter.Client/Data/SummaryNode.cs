﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;

namespace AOCrafter.Client.Data
{
    internal class SummaryNode<T>
    {
        internal SummaryNode( T item )
        {
            this.item = item;
        }

        private T item;
        private int value = 0;
        private int count = 0;

        internal T Item
        {
            get { return this.item; }
        }

        internal int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        internal int Count
        {
            get { return this.count; }
            set { this.count = value; }
        }
    }
}
