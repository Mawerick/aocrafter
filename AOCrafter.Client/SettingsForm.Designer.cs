﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Client
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.tabControl = new Twp.Controls.TabControl();
            this.dbPage = new System.Windows.Forms.TabPage();
            this.dbVersionLabel = new System.Windows.Forms.Label();
            this.gameVersionLabel = new System.Windows.Forms.Label();
            this.dbVersionBox = new System.Windows.Forms.Label();
            this.gameVersionBox = new System.Windows.Forms.Label();
            this.updateDbButton = new System.Windows.Forms.Button();
            this.pathGroupBox = new System.Windows.Forms.GroupBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.pathBox = new System.Windows.Forms.TextBox();
            this.skillsPage = new System.Windows.Forms.TabPage();
            this.setSkillButton = new System.Windows.Forms.Button();
            this.skillValueBox = new System.Windows.Forms.NumericUpDown();
            this.skillKeyBox = new System.Windows.Forms.ComboBox();
            this.skillsList = new Twp.Controls.FillListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.aboutPage = new System.Windows.Forms.TabPage();
            this.htmlPanel = new Twp.Controls.HtmlRenderer.HtmlPanel();
            this.splashBox = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.dbSizeBox = new System.Windows.Forms.Label();
            this.dbSizeLabel = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.dbPage.SuspendLayout();
            this.pathGroupBox.SuspendLayout();
            this.skillsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skillValueBox)).BeginInit();
            this.aboutPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splashBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.dbPage);
            this.tabControl.Controls.Add(this.skillsPage);
            this.tabControl.Controls.Add(this.aboutPage);
            this.tabControl.ItemSize = new System.Drawing.Size(0, 17);
            this.tabControl.Location = new System.Drawing.Point(6, 6);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(392, 391);
            this.tabControl.TabIndex = 0;
            // 
            // dbPage
            // 
            this.dbPage.BackColor = System.Drawing.SystemColors.Control;
            this.dbPage.Controls.Add(this.dbSizeLabel);
            this.dbPage.Controls.Add(this.dbSizeBox);
            this.dbPage.Controls.Add(this.dbVersionLabel);
            this.dbPage.Controls.Add(this.gameVersionLabel);
            this.dbPage.Controls.Add(this.dbVersionBox);
            this.dbPage.Controls.Add(this.gameVersionBox);
            this.dbPage.Controls.Add(this.updateDbButton);
            this.dbPage.Controls.Add(this.pathGroupBox);
            this.dbPage.Location = new System.Drawing.Point(4, 21);
            this.dbPage.Name = "dbPage";
            this.dbPage.Padding = new System.Windows.Forms.Padding(3);
            this.dbPage.Size = new System.Drawing.Size(384, 366);
            this.dbPage.TabIndex = 0;
            this.dbPage.Text = "Database";
            this.dbPage.Paint += new System.Windows.Forms.PaintEventHandler(this.DbPagePaint);
            // 
            // dbVersionLabel
            // 
            this.dbVersionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dbVersionLabel.AutoSize = true;
            this.dbVersionLabel.Location = new System.Drawing.Point(178, 137);
            this.dbVersionLabel.Name = "dbVersionLabel";
            this.dbVersionLabel.Size = new System.Drawing.Size(94, 13);
            this.dbVersionLabel.TabIndex = 4;
            this.dbVersionLabel.Text = "Database Version:";
            // 
            // gameVersionLabel
            // 
            this.gameVersionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gameVersionLabel.AutoSize = true;
            this.gameVersionLabel.Location = new System.Drawing.Point(196, 105);
            this.gameVersionLabel.Name = "gameVersionLabel";
            this.gameVersionLabel.Size = new System.Drawing.Size(76, 13);
            this.gameVersionLabel.TabIndex = 2;
            this.gameVersionLabel.Text = "Game Version:";
            // 
            // dbVersionBox
            // 
            this.dbVersionBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dbVersionBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dbVersionBox.Location = new System.Drawing.Point(278, 132);
            this.dbVersionBox.Name = "dbVersionBox";
            this.dbVersionBox.Size = new System.Drawing.Size(100, 23);
            this.dbVersionBox.TabIndex = 5;
            this.dbVersionBox.Text = "N/A";
            this.dbVersionBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameVersionBox
            // 
            this.gameVersionBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gameVersionBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameVersionBox.Location = new System.Drawing.Point(278, 100);
            this.gameVersionBox.Name = "gameVersionBox";
            this.gameVersionBox.Size = new System.Drawing.Size(100, 23);
            this.gameVersionBox.TabIndex = 3;
            this.gameVersionBox.Text = "N/A";
            this.gameVersionBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // updateDbButton
            // 
            this.updateDbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.updateDbButton.Location = new System.Drawing.Point(6, 63);
            this.updateDbButton.Name = "updateDbButton";
            this.updateDbButton.Size = new System.Drawing.Size(372, 24);
            this.updateDbButton.TabIndex = 1;
            this.updateDbButton.Text = "Update database";
            this.updateDbButton.UseVisualStyleBackColor = true;
            this.updateDbButton.Click += new System.EventHandler(this.OnUpdateDb);
            // 
            // pathGroupBox
            // 
            this.pathGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathGroupBox.Controls.Add(this.browseButton);
            this.pathGroupBox.Controls.Add(this.pathBox);
            this.pathGroupBox.Location = new System.Drawing.Point(6, 6);
            this.pathGroupBox.Name = "pathGroupBox";
            this.pathGroupBox.Size = new System.Drawing.Size(372, 51);
            this.pathGroupBox.TabIndex = 0;
            this.pathGroupBox.TabStop = false;
            this.pathGroupBox.Text = "Anarchy Online install location:";
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.Image = global::AOCrafter.Client.Properties.Resources.browse;
            this.browseButton.Location = new System.Drawing.Point(341, 19);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(25, 25);
            this.browseButton.TabIndex = 1;
            this.browseButton.UseCompatibleTextRendering = true;
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.OnBrowse);
            // 
            // pathBox
            // 
            this.pathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathBox.Enabled = false;
            this.pathBox.Location = new System.Drawing.Point(6, 22);
            this.pathBox.Name = "pathBox";
            this.pathBox.Size = new System.Drawing.Size(329, 20);
            this.pathBox.TabIndex = 0;
            // 
            // skillsPage
            // 
            this.skillsPage.BackColor = System.Drawing.SystemColors.Control;
            this.skillsPage.Controls.Add(this.setSkillButton);
            this.skillsPage.Controls.Add(this.skillValueBox);
            this.skillsPage.Controls.Add(this.skillKeyBox);
            this.skillsPage.Controls.Add(this.skillsList);
            this.skillsPage.Location = new System.Drawing.Point(4, 21);
            this.skillsPage.Name = "skillsPage";
            this.skillsPage.Padding = new System.Windows.Forms.Padding(3);
            this.skillsPage.Size = new System.Drawing.Size(384, 366);
            this.skillsPage.TabIndex = 2;
            this.skillsPage.Text = "My Skills";
            // 
            // setSkillButton
            // 
            this.setSkillButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.setSkillButton.Location = new System.Drawing.Point(322, 341);
            this.setSkillButton.Name = "setSkillButton";
            this.setSkillButton.Size = new System.Drawing.Size(60, 23);
            this.setSkillButton.TabIndex = 3;
            this.setSkillButton.Text = "&Set";
            this.setSkillButton.UseVisualStyleBackColor = true;
            this.setSkillButton.Click += new System.EventHandler(this.OnSetSkill);
            // 
            // skillValueBox
            // 
            this.skillValueBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.skillValueBox.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.skillValueBox.Location = new System.Drawing.Point(256, 343);
            this.skillValueBox.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.skillValueBox.Name = "skillValueBox";
            this.skillValueBox.Size = new System.Drawing.Size(60, 20);
            this.skillValueBox.TabIndex = 2;
            // 
            // skillKeyBox
            // 
            this.skillKeyBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skillKeyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.skillKeyBox.FormattingEnabled = true;
            this.skillKeyBox.Location = new System.Drawing.Point(3, 342);
            this.skillKeyBox.Name = "skillKeyBox";
            this.skillKeyBox.Size = new System.Drawing.Size(247, 21);
            this.skillKeyBox.TabIndex = 1;
            this.skillKeyBox.SelectedIndexChanged += new System.EventHandler(this.OnSkillKeySelected);
            this.skillKeyBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.OnFormatSkillKey);
            // 
            // skillsList
            // 
            this.skillsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skillsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.skillsList.FullRowSelect = true;
            this.skillsList.LargeImageList = this.icons;
            this.skillsList.Location = new System.Drawing.Point(3, 3);
            this.skillsList.MultiSelect = false;
            this.skillsList.Name = "skillsList";
            this.skillsList.Size = new System.Drawing.Size(378, 336);
            this.skillsList.SmallImageList = this.icons;
            this.skillsList.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.skillsList.TabIndex = 0;
            this.skillsList.UseCompatibleStateImageBehavior = false;
            this.skillsList.View = System.Windows.Forms.View.Details;
            this.skillsList.SelectedIndexChanged += new System.EventHandler(this.OnSkillSelected);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Skill";
            this.columnHeader1.Width = 314;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.icons.ImageSize = new System.Drawing.Size(16, 16);
            this.icons.TransparentColor = System.Drawing.Color.Lime;
            // 
            // aboutPage
            // 
            this.aboutPage.BackColor = System.Drawing.SystemColors.Control;
            this.aboutPage.Controls.Add(this.htmlPanel);
            this.aboutPage.Controls.Add(this.splashBox);
            this.aboutPage.Location = new System.Drawing.Point(4, 21);
            this.aboutPage.Name = "aboutPage";
            this.aboutPage.Padding = new System.Windows.Forms.Padding(3);
            this.aboutPage.Size = new System.Drawing.Size(384, 366);
            this.aboutPage.TabIndex = 1;
            this.aboutPage.Text = "About...";
            // 
            // htmlPanel
            // 
            this.htmlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.htmlPanel.AutoScroll = true;
            this.htmlPanel.AutoScrollMinSize = new System.Drawing.Size(351, 197);
            this.htmlPanel.AvoidGeometryAntialias = false;
            this.htmlPanel.AvoidImagesLateLoading = false;
            this.htmlPanel.BackColor = System.Drawing.SystemColors.Window;
            this.htmlPanel.BaseStylesheet = "body {\r\n  margin: 1px;\r\n  padding: 1px;;\r\n}";
            this.htmlPanel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.htmlPanel.Location = new System.Drawing.Point(6, 174);
            this.htmlPanel.Name = "htmlPanel";
            this.htmlPanel.Size = new System.Drawing.Size(372, 185);
            this.htmlPanel.TabIndex = 0;
            this.htmlPanel.Text = resources.GetString("htmlPanel.Text");
            this.htmlPanel.LinkClicked += new System.EventHandler<Twp.Controls.HtmlRenderer.Entities.HtmlLinkClickedEventArgs>(this.OnLinkClicked);
            // 
            // splashBox
            // 
            this.splashBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splashBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splashBox.Image = global::AOCrafter.Client.Properties.Resources.Splash;
            this.splashBox.Location = new System.Drawing.Point(6, 6);
            this.splashBox.Name = "splashBox";
            this.splashBox.Size = new System.Drawing.Size(372, 162);
            this.splashBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.splashBox.TabIndex = 0;
            this.splashBox.TabStop = false;
            this.splashBox.Paint += new System.Windows.Forms.PaintEventHandler(this.AboutPlashPaint);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.closeButton.Location = new System.Drawing.Point(323, 403);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "&Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // dbSizeBox
            // 
            this.dbSizeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dbSizeBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dbSizeBox.Location = new System.Drawing.Point(278, 164);
            this.dbSizeBox.Name = "dbSizeBox";
            this.dbSizeBox.Size = new System.Drawing.Size(100, 23);
            this.dbSizeBox.TabIndex = 6;
            this.dbSizeBox.Text = "N/A";
            this.dbSizeBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dbSizeLabel
            // 
            this.dbSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dbSizeLabel.AutoSize = true;
            this.dbSizeLabel.Location = new System.Drawing.Point(193, 169);
            this.dbSizeLabel.Name = "dbSizeLabel";
            this.dbSizeLabel.Size = new System.Drawing.Size(79, 13);
            this.dbSizeLabel.TabIndex = 7;
            this.dbSizeLabel.Text = "Database Size:";
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 432);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.OnLoad);
            this.tabControl.ResumeLayout(false);
            this.dbPage.ResumeLayout(false);
            this.dbPage.PerformLayout();
            this.pathGroupBox.ResumeLayout(false);
            this.pathGroupBox.PerformLayout();
            this.skillsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skillValueBox)).EndInit();
            this.aboutPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splashBox)).EndInit();
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.ImageList icons;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private Twp.Controls.FillListView skillsList;
        private System.Windows.Forms.ComboBox skillKeyBox;
        private System.Windows.Forms.NumericUpDown skillValueBox;
        private System.Windows.Forms.Button setSkillButton;
        private System.Windows.Forms.TabPage skillsPage;
        private System.Windows.Forms.Label gameVersionBox;
        private System.Windows.Forms.Label dbVersionBox;
        private System.Windows.Forms.Label gameVersionLabel;
        private System.Windows.Forms.Label dbVersionLabel;

        #endregion

        private Twp.Controls.TabControl tabControl;
        private System.Windows.Forms.TabPage dbPage;
        private System.Windows.Forms.TabPage aboutPage;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button updateDbButton;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.TextBox pathBox;
        private System.Windows.Forms.GroupBox pathGroupBox;
        private Twp.Controls.HtmlRenderer.HtmlPanel htmlPanel;
        private System.Windows.Forms.PictureBox splashBox;
        private System.Windows.Forms.Label dbSizeBox;
        private System.Windows.Forms.Label dbSizeLabel;
    }
}