﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Controls;
using Twp.Utilities;

namespace AOCrafter.Client
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            this.titleText = String.Format( "{0} {1}", Application.ProductName, VersionString.Format( VersionFormat.Simple ) );

            this.searchBox = new TradeskillSearchBox();
            this.searchBox.SelectionChanged += this.OnTradeskillSelected;
            this.searchButton.Content = this.searchBox;

            #if DEBUG
            ConsoleWindow console = new ConsoleWindow();
            console.ShowInTaskbar = false;
            console.Show();
            #endif
        }

        private TradeskillSearchBox searchBox;
        private readonly string titleText;

        private void OnLoad( object sender, EventArgs e )
        {
            this.Text = this.titleText;
        }

        private void OnShown( object sender, EventArgs e )
        {
            this.selectionBox.Init();
            this.tradeskillBox.SplitterDistance = Program.Settings.ProcessSplitter;
        }

        private void OnFormClosed( object sender, FormClosedEventArgs e )
        {
            Program.Settings.ProcessSplitter = this.tradeskillBox.SplitterDistance;
        }

        private void OnTradeskillSelected( object sender, EventArgs e )
        {
            this.selectionBox.Tradeskill = this.searchBox.SelectedTradeskill;
            if( this.searchButton.Open )
                this.searchButton.PerformClick();
        }

        private void OnSettingsClicked( object sender, EventArgs e )
        {
            using( SettingsForm form = new SettingsForm() )
            {
                form.ShowDialog();
            }
        }

        private void OnCalculateClicked( object sender, EventArgs e )
        {
            this.Text = String.Format( "{0} :: [{1}]", this.titleText, this.selectionBox.Tradeskill );
            this.tradeskillBox.SetTradeskill( this.selectionBox.Tradeskill, this.selectionBox.QL, this.selectionBox.MaxQL );
            this.splashBox.Visible = false;
        }
    }
}
