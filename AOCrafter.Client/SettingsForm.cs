﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Controls;
using Twp.Controls.HtmlRenderer.Entities;
using Twp.Data.AO;
using Twp.FC.AO;
using Twp.Utilities;

namespace AOCrafter.Client
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
            this.skillKeyBox.DataSource = Enum.GetValues( typeof( SkillName ) );
            this.versionString = String.Format( "Version: {0}", VersionString.Format( VersionFormat.Simple ) );
        }

        private void OnLoad( object sender, EventArgs e )
        {
            this.VerifyPath( Program.Settings.AOPath );
            this.UpdateDbInfo();
            this.UpdateSkills();
        }

#region Database Page

        private void OnBrowse( object sender, EventArgs e )
        {
            string path = Program.Settings.AOPath;
            if( GamePath.Browse( ref path ) )
                this.VerifyPath( path );
        }

        private void VerifyPath( string path )
        {
            if( !GamePath.IsValid( path ) )
            {
                if( !String.IsNullOrEmpty( path ) )
                    Log.Warning( "The selected folder is not a valid Anarchy Online installation." );
                this.pathBox.Text = "**Error**";
                this.updateDbButton.Enabled = false;
                this.gameVersionBox.Text = "N/A";
                return;
            }
            else
            {
                this.pathBox.Text = path;
                this.updateDbButton.Enabled = true;
                Program.Settings.AOPath = path;
                this.gameVersionBox.Text = Twp.Data.AO.DatabaseManager.GetVersion( path );
            }
        }

        private void OnUpdateDb( object sender, EventArgs e )
        {
            string path = Program.Settings.AOPath;
            Log.Debug( "[SettingsForm.OnUpdateDb] Checking path: {0}", path );
            if( !GamePath.IsValid( path ) )
                return;

            Log.Debug( "[SettingsForm.OnUpdateDb] Calling DBManager.UpdateItems( {0}, 10 )", path );
            Core.DatabaseManager.UpdateItems( path, 1, false );
            this.UpdateDbInfo();
            Log.Debug( "[SettingsForm.OnUpdateDb] DBManager.UpdateItems() is done." );
        }

		private void UpdateDbInfo()
		{
            this.dbVersionBox.Text = Core.DatabaseManager.GetValue( "version", Twp.Data.AO.DatabaseManager.ItemsDatabase );
            if( String.IsNullOrEmpty( this.dbVersionBox.Text ) )
                this.dbVersionBox.Text = "N/A";
            FileInfo dbInfo = new FileInfo( Twp.Data.AO.DatabaseManager.ItemsDatabase.FileName );
            if( dbInfo.Exists )
                this.dbSizeBox.Text = ((FileSize) dbInfo.Length).ToString();
            else
                this.dbSizeBox.Text = "N/A";
		}

        private void DbPagePaint( object sender, PaintEventArgs e )
        {
            if( this.DesignMode )
                return;

            Image image = Properties.Resources.AOLogo;
            Rectangle rect = new Rectangle( 0, this.dbPage.Height - image.Height, image.Width, image.Height );
            e.Graphics.DrawImageUnscaledAndClipped( image, rect );
        }

#endregion

#region MySkills Page
        
        private void UpdateSkills()
        {
            this.skillsList.BeginUpdate();
            this.skillsList.Items.Clear();
            foreach( Skill skill in Program.MySkills )
            {
                this.AddSkillToList( skill );
            }
            this.skillsList.EndUpdate();
        }

        private void AddSkillToList( Skill skill )
        {
            ListViewItem item = new ListViewItem();
            item.Text = Skill.GetName( skill.Key );
            item.SubItems.Add( skill.Value.ToString() );
            item.ImageIndex = this.AddSkillIcon( Skill.GetIcon( skill.Key ) );
            item.Tag = skill;
            this.skillsList.Items.Add( item );
        }

        private void RemoveSkillFromList( Skill skill )
        {
            ListViewItem item = FindSkillInList( skill );
            if( item != null )
                this.skillsList.Items.Remove( item );
        }

        private ListViewItem FindSkillInList( Skill skill )
        {
            foreach( ListViewItem item in this.skillsList.Items )
            {
                if( item.Tag.Equals( skill ) )
                    return item;
            }
            return null;
        }

        private int AddSkillIcon( int id )
        {
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
            return this.icons.Images.IndexOfKey( id.ToString() );
        }

        private void OnSkillSelected( object sender, EventArgs e )
        {
            if( this.skillsList.SelectedItems.Count <= 0 )
                return;

            ListViewItem item = this.skillsList.SelectedItems[0];
            this.skillKeyBox.SelectedItem = ( item.Tag as Skill ).Key;
        }

        private void OnSkillKeySelected( object sender, EventArgs e )
        {
            if( this.skillKeyBox.SelectedIndex < 0 )
                return;

            Skill skill = Program.MySkills[(SkillName) this.skillKeyBox.SelectedItem];
            if( skill != null )
                this.skillValueBox.Value = skill.Value;
        }

        private void OnFormatSkillKey( object sender, ListControlConvertEventArgs e )
        {
            e.Value = Skill.GetName( (SkillName) e.Value );
        }

        private void OnSetSkill( object sender, EventArgs e )
        {
            SkillName key = (SkillName) this.skillKeyBox.SelectedItem;
            Skill skill = Program.MySkills[key];
            if( skill == null )
            {
                if( this.skillValueBox.Value == 0m )
                    return;

                skill = new Skill();
                skill.Key = key;
                Program.MySkills.Add( skill );
                this.AddSkillToList( skill );
            }
            if( this.skillValueBox.Value == 0m )
            {
                Program.MySkills.Remove( skill );
                this.RemoveSkillFromList( skill );
            }
            else
            {
                skill.Value = this.skillValueBox.Value;
                ListViewItem item = this.FindSkillInList( skill );
                item.SubItems[1].Text = skill.Value.ToString();
            }
        }

#endregion

#region About Page

        private readonly string versionString;

        private void AboutPlashPaint( object sender, PaintEventArgs e )
        {
            GraphicsState state = e.Graphics.Save();
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            SizeF sizeF = e.Graphics.MeasureString( this.versionString, this.Font );
            Size size = sizeF.ToSize();
            Rectangle rect = new Rectangle( this.splashBox.Width - size.Width - 12, 6, size.Width + 4, size.Height + 5 );

            Color backColor = Color.FromArgb( 150, 25, 25, 30 );
            Color borderColor = Color.FromArgb( 180, 50, 50, 60 );
            Color foreColor = Color.FromArgb( 220, 150, 130, 100 );
            using( GraphicsPath path = RoundedRectangle.Create( rect, 6 ) )
            {
                using( SolidBrush brush = new SolidBrush( backColor ) )
                {
                    e.Graphics.FillPath( brush, path );
                }
                using( Pen pen = new Pen( borderColor ) )
                {
                    e.Graphics.DrawPath( pen, path );
                }
                rect.Offset( 3, 3 );
                using( SolidBrush brush = new SolidBrush( foreColor ) )
                {
                    e.Graphics.DrawString( this.versionString, this.Font, brush, rect );
                }
            }
            e.Graphics.Restore( state );
        }

        private void OnLinkClicked( object sender, HtmlLinkClickedEventArgs e )
        {
            System.Diagnostics.Process.Start( e.Link );
        }

#endregion
    }
}
