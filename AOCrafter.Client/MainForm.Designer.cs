﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.settingsButton = new System.Windows.Forms.Button();
            this.searchButton = new Twp.Controls.PopupButton();
            this.line1 = new Twp.Controls.Line();
            this.selectionBox = new AOCrafter.Client.SelectionBox();
            this.tradeskillBox = new AOCrafter.Client.TradeskillBox();
            this.splashBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splashBox)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsButton
            // 
            this.settingsButton.Image = global::AOCrafter.Client.Properties.Resources.Settings;
            this.settingsButton.Location = new System.Drawing.Point(429, 49);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(36, 36);
            this.settingsButton.TabIndex = 2;
            this.toolTip.SetToolTip(this.settingsButton, "Settings.");
            this.settingsButton.UseVisualStyleBackColor = true;
            this.settingsButton.Click += new System.EventHandler(this.OnSettingsClicked);
            // 
            // searchButton
            // 
            this.searchButton.Image = ((System.Drawing.Image)(resources.GetObject("searchButton.Image")));
            this.searchButton.Location = new System.Drawing.Point(429, 9);
            this.searchButton.Name = "searchButton";
            this.searchButton.PopupDirection = Twp.Controls.PopupDirection.DownLeft;
            this.searchButton.Size = new System.Drawing.Size(36, 36);
            this.searchButton.TabIndex = 1;
            this.toolTip.SetToolTip(this.searchButton, "Search for items.");
            this.searchButton.UseVisualStyleBackColor = true;
            // 
            // line1
            // 
            this.line1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.line1.Location = new System.Drawing.Point(0, 90);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(474, 10);
            this.line1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // selectionBox
            // 
            this.selectionBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectionBox.Location = new System.Drawing.Point(9, 9);
            this.selectionBox.Name = "selectionBox";
            this.selectionBox.Size = new System.Drawing.Size(414, 76);
            this.selectionBox.TabIndex = 0;
            this.selectionBox.Tradeskill = null;
            this.selectionBox.CalcClicked += new System.EventHandler(this.OnCalculateClicked);
            // 
            // tradeskillBox
            // 
            this.tradeskillBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tradeskillBox.Location = new System.Drawing.Point(6, 103);
            this.tradeskillBox.Name = "tradeskillBox";
            this.tradeskillBox.Size = new System.Drawing.Size(462, 403);
            this.tradeskillBox.TabIndex = 3;
            // 
            // splashBox
            // 
            this.splashBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splashBox.Image = global::AOCrafter.Client.Properties.Resources.Splash;
            this.splashBox.Location = new System.Drawing.Point(0, 95);
            this.splashBox.Name = "splashBox";
            this.splashBox.Size = new System.Drawing.Size(474, 418);
            this.splashBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.splashBox.TabIndex = 6;
            this.splashBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 512);
            this.Controls.Add(this.splashBox);
            this.Controls.Add(this.settingsButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.selectionBox);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.tradeskillBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "AOCrafter";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnFormClosed);
            this.Load += new System.EventHandler(this.OnLoad);
            this.Shown += new System.EventHandler(this.OnShown);
            ((System.ComponentModel.ISupportInitialize)(this.splashBox)).EndInit();
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.PictureBox splashBox;
        private AOCrafter.Client.TradeskillBox tradeskillBox;

        #endregion

        private Twp.Controls.Line line1;
        private SelectionBox selectionBox;
        private Twp.Controls.PopupButton searchButton;
        private System.Windows.Forms.Button settingsButton;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

