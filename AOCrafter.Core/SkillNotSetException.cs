﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Runtime.Serialization;

namespace AOCrafter.Core
{
	/// <summary>
	/// Gets thrown when calculating a tradeskill, and a required skill is not set.
	/// </summary>
	[Serializable]
	public class SkillNotSetException : Exception
	{
		/// <summary>
		/// Gets the skill name that was not set.
		/// </summary>
		public SkillName Skill
		{
		    get { return this.skill; }
		    set { this.skill = value; }
		}
		private SkillName skill;

		/// <summary>
		/// Initializes a new instance of the <see cref="SkillNotSetException"/> class.
		/// </summary>
		public SkillNotSetException() : base() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SkillNotSetException"/> class with a specified skill.
		/// </summary>
		public SkillNotSetException( SkillName skill ) : base()
		{
		    this.skill = skill;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SkillNotSetException"/> class with a specified error message.
		/// </summary>
		/// <param name="message">The message that descrives the error.</param>
		public SkillNotSetException( string message ) : base( message ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SkillNotSetException"/> class with a specified error message and skill.
		/// </summary>
		/// <param name="message">The message that descrives the error.</param>
		/// <param name="line">The line in which the error occured.</param>
		public SkillNotSetException( string message, SkillName skill ) : base( message )
		{
			this.skill = skill;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SkillNotSetException"/> class with a specified error message
		/// and a reference to the inner exception that is the cause of this error.
		/// </summary>
		/// <param name="message">The message that descrives the error.</param>
		/// <param name="innerException">The exception that is the cause of the current exception,
		///  or a null reference if no inner exception is specivied</param>
		public SkillNotSetException( string message, Exception innerException ) : base( message, innerException ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="IniParseException"/> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source ordestination.</param>
		protected SkillNotSetException( SerializationInfo info, StreamingContext context ) : base( info, context ) { }
	}
}
