﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace AOCrafter.Core
{
    // Call as IntArray(byte[],int)...
    [SQLiteFunction( Name = "IntArray", FuncType = FunctionType.Scalar, Arguments = 2 )]
    public class IntArray : SQLiteFunction
    {
        public IntArray()
        {
        }

        public override object Invoke( object[] args )
        {
            int count = 0;
            int[] items = IntArray.FromByteArray( (byte[]) args[0] );
            foreach( int item in items )
            {
                if( item == Convert.ToInt32( args[1] ) )
                    count++;
            }
            return count;
        }

        public static byte[] ToByteArray( int[] items )
        {
            MemoryStream ms = new MemoryStream();
            byte[] bytes = BitConverter.GetBytes( items.Length );
            ms.Write( bytes, 0, bytes.Length );
            foreach( int item in items )
            {
                bytes = BitConverter.GetBytes( item );
                ms.Write( bytes, 0, bytes.Length );
            }
            bytes = ms.ToArray();
            ms.Dispose();
            return bytes;
        }

        public static int[] FromByteArray( byte[] array )
        {
            List<int> items = new List<int>();
            using( MemoryStream ms = new MemoryStream( array ) )
            {
                byte[] bytes = new byte[4];
                ms.Read( bytes, 0, 4 );
                int count = BitConverter.ToInt32( bytes, 0 );
                for( int i = 0; i < count; i++ )
                {
                    ms.Read( bytes, 0, 4 );
                    int item = BitConverter.ToInt32( bytes, 0 );
                    items.Add( item );
                }
            }
            return items.ToArray();
        }
    }
}
