﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace AOCrafter.Core
{
    public class DatabaseStatistics
    {
        public DatabaseStatistics( string text, int count )
        {
            this.text = text;
            this.count = count;
        }

        private string text;
        private int count;

        public string Text
        {
            get { return this.text; }
        }

        public int Count
        {
            get { return this.count; }
        }

        public override string ToString()
        {
            return String.Format( "{0}: {1}", this.text, this.count );
        }
    }
}
