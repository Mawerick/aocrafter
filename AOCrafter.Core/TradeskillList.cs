﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public class TradeskillList : List<Tradeskill>
    {
        public new void Add( Tradeskill tradeskill )
        {
            if( !base.Contains( tradeskill ) )
                base.Add( tradeskill );
            else
                Log.Debug( "[TradeskillCollection.Add] Tradeskill already in list: {0}", tradeskill.Id );
        }
//
//        public byte[] ToByteArray()
//        {
//            List<int> ids = new List<int>();
//            foreach( Tradeskill tradeskill in this )
//            {
//                ids.Add( tradeskill.Id );
//            }
//            return IntArray.ToByteArray( ids.ToArray() );
//        }
//
//        public static TradeskillList FromByteArray( byte[] array )
//        {
//            TradeskillList items = new TradeskillList();
//            int[] ids = IntArray.FromByteArray( array );
//            foreach( int id in ids )
//            {
//                Tradeskill item = Tradeskill.Load( id );
//                if( item != null )
//                    items.Add( item );
//            }
//            return items;
//        }
    }
}
