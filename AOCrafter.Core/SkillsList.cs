﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.IO;

namespace AOCrafter.Core
{
    public class SkillsList : BindingList<Skill>
    {
        public SkillsList() : base()
        {
        }

        public bool Contains( SkillName key )
        {
            foreach( Skill skill in this )
            {
                if( skill.Key == key )
                    return true;
            }
            return false;
        }

        public Skill this[SkillName key]
        {
            get
            {
                foreach( Skill skill in this )
                {
                    if( skill.Key == key )
                        return skill;
                }
                return null;
            }
        }

        public byte[] ToByteArray()
        {
            MemoryStream ms = new MemoryStream();
            byte[] bytes = BitConverter.GetBytes( this.Count );
            ms.Write( bytes, 0, bytes.Length );
            foreach( Skill item in this )
            {
                bytes = BitConverter.GetBytes( (int) item.Key );
                ms.Write( bytes, 0, bytes.Length );
                bytes = BitConverter.GetBytes( Decimal.ToSingle( item.Value ) );
                ms.Write( bytes, 0, bytes.Length );
            }
            bytes = ms.ToArray();
            ms.Dispose();
            return bytes;
        }

        public static SkillsList FromByteArray( byte[] array )
        {
            SkillsList items = new SkillsList();
            using( MemoryStream ms = new MemoryStream( array ) )
            {
                int size = Math.Max( sizeof( int ), sizeof( float ) );
                byte[] bytes = new byte[size];
                ms.Read( bytes, 0, sizeof( int ) );
                int count = BitConverter.ToInt32( bytes, 0 );
                for( int i = 0; i < count; i++ )
                {
                    Skill item = new Skill();
                    ms.Read( bytes, 0, sizeof( int ) );
                    item.Key = (SkillName) BitConverter.ToInt32( bytes, 0 );
                    ms.Read( bytes, 0, sizeof( float ) );
                    item.Value = Convert.ToDecimal( BitConverter.ToSingle( bytes, 0 ) );
                    items.Add( item );
                }
            }
            return items;
        }
    }
}
