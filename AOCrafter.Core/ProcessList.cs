﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public class ProcessList : List<Process>
    {
        /// <summary>
        /// Adds a <see cref="Process" /> to the end of the list, unless the Process is already in the list.
        /// </summary>
        /// <param name="process">The Process to be added.</param>
        public new void Add( Process process )
        {
            if( !base.Contains( process ) )
                base.Add( process );
            else
                Log.Debug( "[ProcessCollection.Add] Process already in collection: {0}", process.Id );
        }

        /// <summary>
        /// Converts the ProcessCollection to a byte[] for database storage.
        /// </summary>
        /// <returns>A byte[] containing the IDs of the Processes in the collection.</returns>
        public byte[] ToByteArray()
        {
            List<int> ids = new List<int>();
            foreach( Process process in this )
            {
                ids.Add( process.Id );
            }
            return IntArray.ToByteArray( ids.ToArray() );
        }

        /// <summary>
        /// Converts a byte[] to a ProcessCollection.
        /// </summary>
        /// <param name="array">A byte[] containing Process IDs.</param>
        /// <returns>A ProcessCollection containing the Processes referred to in the byte[]</returns>
        public static ProcessList FromByteArray( byte[] array )
        {
            ProcessList items = new ProcessList();
            int[] ids = IntArray.FromByteArray( array );
            foreach( int id in ids )
            {
                Process item = Process.Load( id );
                if( item != null )
                    items.Add( item );
            }
            return items;
        }
    }
}
