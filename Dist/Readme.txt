=========
AOCrafter
=========

AOCrafter is a small tradeskill calculator that helps you figure out all the 
math involved with tradeskills.

Requires .Net 2.0

Features:
=========

 - Shows step-by-step breakdown with required tools, items and skills of many 
   known tradeskill processes.
 - Calculates the minimum required skills and items needed for the desired 
   Quality Level (QL) of a process.
 - Shows a summary of the minimum skills and QL of items needed for completing 
   the entire process, including amount of items needed.
 - Calculates maximum possible QL of process based on your skills.


Installation:
=============

Installer:
 - Download the file "AOCrafter-0.9.0.exe" to wherever you store your stuff.
 - Run the installer and follow the instructions given.

Manual installation:
 - Download the file "AOCrafter-0.9.0.zip" to wherever you store your stuff.
 - Extract the content to a desired location (i.e. "C:\Program Files\AOCrafter").
 - Move "AOCrafter.db3" to "C:\ProgramData\WrongPlace.Net\AOCrafter\"
 - (Optional) Create a shortcut to the file "AOCrafter.exe" on your desktop.


Links:
======

 http://www.wrongplace.net/            News site
 http://aocrafter.wrongplace.net/      Project site
 http://aodevs.com/                    Release site for AO 3rd party tools
 http://www.anarchy-online.com/        Official Anarchy Online site

Credits:
========

Author: Mawerick (mawerick@wrongplace.net)
(Please write in english only)
